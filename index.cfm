<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<title>Dena'ina Qenaga -- A Resource for the Dena'ina Language</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Dena'ina Qenaga is a resource for learning about  Dena'ina, an indigenous language spoken in Southcentral Alaska." />
<link rel="stylesheet" type="text/css" href="qenaga.css" />
<style type="text/css">
/* @import "qenaga.css"; */
/* hide cluster map for now */
#clustrMapsImg
{
	display: none;
}
</style>
<meta http-equiv="imagetoolbar" content="no" />
</head>

<cfquery name="getids" datasource="emeld">
select ID from DATA_WORDS
</cfquery>
<cfset start_day = DateDiff("y","03/01/2006",Now())>
<cfset week = Round((start_day / 7) mod 225)>
<cfset L_wordIDs = ValueList(getids.id)>
<cfset takeword = ListGetAt(L_wordIDs, #week#)>

<cfquery name="getword" datasource="emeld">
select
	WORD,
	GLOSS,
	AUDIO
from DATA_WORDS
where ID=#takeword#
</cfquery>

<cfset audiofile = "audio/words/" & #getword.AUDIO#>


<body>


	<div id="banner">
	<div id="nav">
		<ul>
			<li><a href="people.html">People</a></li>
			<li><a href="history.html">History</a></li>
			<li><a href="literature.html">Literature</a></li>
			<li><a href="learning.html">Learning</a></li>
			<li><a href="language.html" >Language</a></li>
			<li><a href="archive/index.cfm">Archive</a></li>
			<li><a href="/">Home</a></li>


		</ul>
	</div>
	</div>
	<div id="leftblock">

	<!-- Announcements table -->
		<table width="80%" align="center" style=" background-color: white; border:1px solid #35331C;" >
			<tr>
				<td align="center"><font style="color:#663300; font-size:10pt; ">
				Welcome back! <br/>Thanks to our friends at the <a href="http://linguistlist.org">LINGUIST List</a>
				the Dena'ina Qenaga website is now back online.
				<br/></br/>

                [<a href="news.html">more news</a>]</font>
				</td>


		</table>
		<br />
		<!-- Featured Word Table -->


		<table width="80%" align="center" style="border:1px solid #35331C;">
			<tr><td align="center"><font style="color:#663300; font-size:10pt; "><b>Featured Word</b><br />Click to listen!</font></td></tr>

			 <tr><td align="center"><font style="color:#CC6600; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:18px; "><a href="wod-today.cfm?id=<cfoutput>#takeword#</cfoutput>"><cfoutput>#getword.word#</cfoutput></a></font></td></tr>

			<tr><td align="center"><font style="color:#CC6600; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; ">'<cfoutput>#getword.gloss#</cfoutput>'</font></td></tr>
			<tr><td align="center"><font style="color:#663300; font-size:10pt; ">or visit the<br/><a href="wod.cfm">Featured Word Archive</a></font>
		</table>
				<br />
	<!-- Search Table -->

<form name="quick" action="archive/search-2.cfm" method="post">
<input name="searchtype" type="hidden" value="0">
<table width="80%" align="center" style=" background-color:#E8E7D9; border:1px solid #35331C;">
<td align="center"><font style="color:#666600; font-size:12pt; "><b>Search Archive</b></font>
				</td>
			</tr>
<tr>
<td align="center"><input name="keyword" type="text" width="80" /></td></tr>
<tr><td align="center"><input  type="submit" value="Search"></td></tr>
</table>
</form>
		<br />
		<div id="left-nav">
		<ul>
			<li><a href="news.html">Qenek (News)</a></li>
			<li><a href="credits.html">About this Site</a></li>
			<li><a href="sitemap.html">Sitemap</a></li>
			<li><a href="license.html">License</a></li>
			<li><a href="feedback.cfm">Feedback</a></li>

		</ul>
		</div>
	</div>

<div id="rightblock">

		<h1>Nagh Nduninyu! Welcome to Dena'ina Qenaga</h1>
<p>The Dena'ina Qenaga website is a web-based resource for the Dena'ina Athabascan language. This site is designed to provide information about the Dena'ina language, including information about language structure (grammar, pronunciation, spelling, etc.); information about learning the Dena'ina language (phrases and conversations, stories, etc.); and information about community language revitalization programs. This phase of website development is very much in its initial, beginning stages.  We welcome ideas for additional content.
</p>

<h2>Dena'ina Qenaga Digital Archive</h2>
<p>
One of the primary goals of this site is to provide access to Dena'ina archival materials currently housed at the Alaska Native Language Archive. The need for access to materials was stated quite eloquently by <a href="people.html#andrewballuta">Andrew Balluta</a> at a language workshop held at the Alaska Native Heritage Center in February 2004.
<table border="0" cellpadding="10" align="center" width="75%"><tr valign="center">
<td>"You know, all these recordings ... if we don't get it out and learn about it, where are we going to learn from? These are old recordings. We want to get it out and teach our younger children what the elder people are talking about. I think that's a very good idea for getting it free so we can listen to them."
<br/><br/>--Andrew Balluta (1930-2011)</td>
<td><img src="images/balluta.jpg" width="150"  hspace="5"  /></td>
</tr></table>
<p>
 The web-based archive currently provides access to a database of more than two hundred documents and more than two hundred audio recordings relating to the Dena'ina language. Original copies of these materials are housed at the <a href="http://www.uaf.edu/anla/">Alaska Native Language Archive</a>. Digital copies of some of this material can be downloaded directly from this site. In some cases, access restrictions prohibit web-based access to digital materials. However, digital copies may still be available directly from the Alaska Native Language Center.
 </p>


<p>Initial development of this site was
 supported by the <i>Dena'ina Archiving, Training and Access</i> (DATA) project,
 funded by <a href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=0326805">US National Science Foundation grant NSF-OPP</a>.
 Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author and do not necessarily reflect the views of the National Science Foundation.
 Site hosting is provided by the <a href="http://linguistlist.org">LINGUIST List</a>. Your <a href="http://funddrive.linguistlist.org/donate/">donattion to the LINGUIST List</a> will help to support the continuation of this site.
 See  <a href="credits.html" target="new">About this Site</a>  for additional information.
 </p>

 <br/>
<p align="center">
<a href="http://www.nsf.gov"><img src="images/nsf1.gif" hspace="10" width="100" height="100"/></a>
<a href="http://linguistlist.org"><img src="images/linguistlist_logo.png" height="100" /></a>
</p>

<br />
</div>


<div id="footer">

<table width="90%" align="center">
	<tr><td><br /><hr /></td></tr>
	<tr>
		<td align="justify"><p class="fineprint">
Materials on this site are copyrighted by the original authors, the speakers whose voices are recorded, and the Alaska
Native Language Archive. Materials may be used freely for non-commercial, educational purposes as specified in the <a href="license.html">license
agreement</a>. <a href="http://www.uaf.edu/anla/">Alaska Native Language Archive</a> materials made available through the Dena'ina Qenaga Digital Archive may be subject to more
restrictive conditions of use as specified by the original depositors.
</p>

<p  class="fineprint">
Dena'ina Qenaga website copyright &copy; 2004-<cfoutput>#Year(NOW())#</cfoutput>. Suggestions for future development are welcome.
If you have questions or comments about this site, please contact
the DATA Project: denaina [dot] qenaga [at] gmail [dot] com.</p>
</td></tr></table>

<br/><br/>

<!---
<p align="center">
  <a
href="http://www3.clustrmaps.com/counter/maps.php?url=http://qenaga.org"
id="clustrMapsLink"><img
src="http://www3.clustrmaps.com/counter/index2.php?url=http://qenaga.org"
style="border:0px;" alt="Locations of visitors to this page"
title="Locations of visitors to this page" id="clustrMapsImg"
onError="this.onError=null;
this.src='http://www2.clustrmaps.com/images/clustrmaps-back-soon.jpg';
document.getElementById('clustrMapsLink').href='http://www2.clustrmaps.com'"
/></a></p>
--->
</div>


	</body>
</html>
