User-agent: *
Disallow: /archive/
Disallow: /images/
Disallow: /texts/
Disallow: /test/
Disallow: /docs/
Disallow: /audio/

# slow down Yahoo
User-agent: Slurp
Crawl-delay: 10

# slow down good bots
User-agent: * 
Crawl-Delay: 600
