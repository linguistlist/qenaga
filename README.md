# quenaga.org

## Project

The Dena'ina Qenaga website (http://qenaga.org) was created using ColdFusion as part of the Dena'ina Archiving, Training, and Access Project (2004-2006), a collaboration of the Alaska Native Language Center and the LinguistLIST funded by US National Science Foundation grant OPP-0326805. In January 2017 the archived site was migrated to Lucee, hosted at LinguistLIST.

## Database

The database needed for this is `emeld`. It should be the same as we have set up for LINGUIST List Lucee server. Please look at the README file for LL repository.

## Media Files

Please note that the huge sound files and documents are not in the repository. They are in the backup drive. The folders excluded from the repository are:

    - archive/tennenbaum
    - archive/docs/
    - archive/alignment-files/
    - audio/
    - fish/audio/
    - sava/audio/
    - texts/audio/

As of 2018-03-05 media fiels are hosted at:
  http://qenaga.kaipumakani.org/media

Please check `.gitignore` for the list of excluded file types.

### kq

It seems like /kq/index.html is redirecting to http://chinook.kpc.alaska.edu/%7Eifasb/ So we don't really need anything in /kq except index.html

I have excluded these folders from the repo:

    - kq/audio/
    - kq/pages/
    - kq/images/
