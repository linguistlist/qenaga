<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <!-- Created by Sadie Williams October 2005 -->
<html>
<head>
  <title>Dena'ina Qenaga -- Feedback</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="qenaga.css" />
  <meta http-equiv="imagetoolbar" content="no" />
</head>

<body>
	<div id="banner">
	<div id="nav">
		<ul>
			<li><a href="people.html">People</a></li>
			<li><a href="history.html">History</a></li>
			<li><a href="literature.html">Literature</a></li>
			<li><a href="learning.html">Learning</a></li>
			<li><a href="language.html" >Language</a></li>
			<li><a href="archive/index.cfm">Archive</a></li>
			<li><a href="/">Home</a></li>


		</ul>
	</div>
	</div>
	<div id="leftblock">
    <div align="center">
	  	<img src="images/sitemap.jpg" alt="Original artwork by Dale DeArmond, used with permission by the Alaska Native Language Center">
	  </div>
	  	<br />
	  <div id="left-nav">
	  	<ul>
	  		<li><a href="news.html">Qenek (News)</a></li>
	  		<li><a href="credits.html">About this Site</a></li>
	  		<li><a href="sitemap.html">Sitemap</a></li>
	  		<li><a href="license.html">License</a></li>
	  		<li><a href="feedback.cfm" id="current">Feedback</a></li>
	  	</ul>
	  </div>
	</div>

	<div id="rightblock">
    Please send your feedback to denaina.qenaga [at] gmail.com

  </div>

<table width="90%" align="center">
	<tr><td><br /><hr /></td></tr>
	<tr>
		<td align="justify"><p class="fineprint">
Materials on this site are copyrighted by the original authors, the speakers whose voices are recorded, and the Alaska
Native Language Archive. Materials may be used freely for non-commercial, educational purposes as specified in the <a href="license.html">license
agreement</a>. Alaska Native Language Archive materials made available through the Dena'ina Qenaga Digital Archive may be subject to more
restrictive conditions of use as specified by the original depositors.
</p>
<p  class="fineprint">
Dena'ina Qenaga website copyright &copy; 2004-<cfoutput>#Year(NOW())#</cfoutput>. Suggestions for future development are welcome. If you have questions or comments about this site, please contact
the DATA Project: denaina [dot] qenaga [at] gmail [dot] com
</p>
		</td>
	</tr>
</table>

</body>
</html>
