<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <!-- Created by Sadie Williams October 2005 -->
<html>
<head>
	<title>Dena'ina Qenaga -- Error</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="http://www.qenaga.org/qenaga.css" />
<meta http-equiv="imagetoolbar" content="no" />
</head>

<body>
	<div id="banner">
	<div id="nav">
		<ul>
			<li><a href="people.html">People</a></li>
			<li><a href="history.html">History</a></li>
			<li><a href="literature.html">Literature</a></li>
			<li><a href="learning.html">Learning</a></li>
			<li><a href="language.html" >Language</a></li>
			<li><a href="archive/index.cfm">Archive</a></li>
			<li><a href="/">Home</a></li>


		</ul>
	</div>
	</div>
	<div id="fullpage">
		<cfmail to="#cferror.MailTo#" from="denaina.qenaga@gmail.com" subject="error">

An exception has occurred with qenaga.

Details:::::::

Browser: #cferror.Browser#
DateTime: #cferror.DateTime#
Detail: #cferror.Detail#
Diagnostics: #cferror.Diagnostics#
Error Code: #cferror.ErrorCode#
Extended Info:  #cferror.ExtendedInfo#
HTTPREFERER: #cferror.HTTPReferer#
Mailto:  #cferror.MailTo#
Message: #cferror.Message#
Query String: #cferror.QueryString#
Remote Address: #cferror.RemoteAddress#
Template: #cferror.Template#
Type: #cferror.Type#

Generated Content: #cferror.GeneratedContent#
		</cfmail>

<p>An error has occurred while trying to process your request.  We apologize for the inconvenience.  The error has been logged and an e-mail notifying the webmaster has automatically been sent.</p>
<p>Please email questions or comments to <a href="mailto:<cfoutput>#cferror.MailTo#">#cferror.Mailto#</cfoutput></a></p>
</div>
</body>
</html>
