<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<title>Dena'ina Qenaga -- A Resource for the Dena'ina Language</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
<link rel="stylesheet" type="text/css" href="qenaga.css" />
<meta http-equiv="imagetoolbar" content="no" />
</head>

<cfquery name="getids" datasource="emeld">
select ID from DATA_WORDS 
</cfquery>
<cfset start_day = DateDiff("y","03/01/2006",Now())>
<cfset week = (start_day / 7)>
<cfset week = Round(#week#)>
<cfset L_wordIDs = ValueList(getids.id)>
<cfset takeword = ListGetAt(L_wordIDs, #week#)>

<cfquery name="getword" datasource="emeld">
select 
	WORD,
	GLOSS,
	AUDIO
from DATA_WORDS
where ID=#takeword#
</cfquery>

<cfset audiofile = "audio/words/" & #getword.AUDIO#>


<body>


	<div id="banner">
	<div id="nav">
		<ul>
			<li><a href="people.html">People</a></li>
			<li><a href="history.html">History</a></li>
			<li><a href="literature.html">Literature</a></li>
			<li><a href="learning.html">Learning</a></li>
			<li><a href="language.html" >Language</a></li>
			<li><a href="archive/index.cfm">Archive</a></li>	
			<li><a href="/">Home</a></li>
			
			
		</ul>
	</div>
	</div>
	<div id="leftblock">
	
	<!-- Announcements table -->
		<table width="80%" align="center" style=" background-color: white; border:1px solid #35331C;" >
			<tr>
				<td align="center"><font style="color:#663300; font-size:10pt; "><a href="http://aprn.org/2007/10/08/50-year-old-recordings-surface-in-new-denaina-cd/">
                <b>Dena'ina music/interview on Alaska Public Radio Network.</b></a>
                <br /><br />
               <a href="news.html"><b><em>Dena'ina Topical Dictionary</em> by James Kari now available.</b></a>
                
                
                <br /><br />
                <b><a href="http://web.kpc.alaska.edu/denaina/"><em>"Kahtnuhtana Qenaga</em>/The Kenai People's Language" website now online!</b></a><br /><br />
                
                
                <b><a href="news.html">Click for more news.</a></b></font>
				</td>
			
			
		</table>
		<br />
		<!-- Featured Word Table -->
		

		<table width="80%" align="center" style="border:1px solid #35331C;">
			<tr><td align="center"><font style="color:#663300; font-size:10pt; "><b>Featured Word</b><br />Click to listen!</font></td></tr>
			
			 <tr><td align="center"><font style="color:#CC6600; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:18px; "><a href="wod-today.cfm?id=<cfoutput>#takeword#</cfoutput>"><cfoutput>#getword.word#</cfoutput></a></font></td></tr>
			
			<tr><td align="center"><font style="color:#CC6600; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; ">'<cfoutput>#getword.gloss#</cfoutput>'</font></td></tr> 
			<tr><td align="center"><font style="color:#663300; font-size:10pt; ">or visit the<br/><a href="wod.cfm">Featured Word Archive</a></font>		
		</table>
				<br />
	<!-- Search Table -->
	
<form name="quick" action="archive/search-2.cfm" method="post">
<input name="searchtype" type="hidden" value="0">		
<table width="80%" align="center" style=" background-color:#E8E7D9; border:1px solid #35331C;">
<td align="center"><font style="color:#666600; font-size:12pt; "><b>Search Archive</b></font>
				</td>
			</tr>
<tr>
<td align="center"><input name="keyword" type="text" width="80" /></td></tr>
<tr><td align="center"><input  type="submit" value="Search"></td></tr>
</table>
</form>
		<br />
		<div id="left-nav">
		<ul>
			<li><a href="news.html">Qenek (News)</a></li>
			<li><a href="credits.html">About this Site</a></li>
			<li><a href="sitemap.html">Sitemap</a></li>
			<li><a href="license.html">License</a></li>
			<li><a href="feedback.cfm">Feedback</a></li>
			
		</ul>
		</div>
	</div>

<div id="rightblock">
		
		<h1>Nagh Nduninyu! Welcome to Dena'ina Qenaga</h1>
<p>The Dena'ina Qenaga website is a web-based resource for the Dena'ina Athabascan language. This site is designed to provide information about the Dena'ina language, including information about language structure (grammar, pronunciation, spelling, etc.); information about learning the Dena'ina language (phrases and conversations, stories, etc.); and information about community language revitalization programs. This phase of website development is very much in its initial, beginning stages.  We welcome ideas for additional content.
</p>

<h2>Dena'ina Qenaga Digital Archive</h2>
<p>
One of the primary goals of this site is to provide access to Dena'ina archival materials currently housed at the Alaska Native Language Archive. The need for access to materials was stated quite eloquently at a language workshop held at the Alaska Native Heritage Center in February 2004.
<ul>
"You know, all these recordings ... if we don't get it out and learn about it, where are we going to learn from? These are old recordings. We want to get it out and teach our younger children what the elder people are talking about. I think that's a very good idea for getting it free so we can listen to them."
</ul>
<p>
 <img src="images/anlc.jpg" width="200" height="150" hspace="5" align="right" />The web-based archive currently provides access to a database of more than two hundred documents and more than two hundred audio recordings relating to the Dena'ina language. Original copies of these materials are housed at the Alaska Native Language Center. Digital copies of some of this material can be downloaded directly from this site. In some cases, access restrictions prohibit web-based access to digital materials. However, digital copies may still be available directly from the Alaska Native Language Center. 
 </p>

<p><br/>
  </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
    <!--- 
<p align="center">
<hr width="50%" /><br />
This site is optimized for use with the Firefox web browser. 
<a href="http://www.spreadfirefox.com/?q=affiliates&id=0&t=70"><img border="0" alt="Get Firefox!" title="Get Firefox!" src="http://sfx-images.mozilla.org/affiliates/Buttons/88x31/get.gif"/></a>

<br/>
  </p>
 ---> 
  
  <br />  
</p>
</div>

<!--- footer for this page slightly different from that used for other pages --->
<div id="footer">
<hr />
<p>
Website development is supported by the Dena'ina Archiving, Training and Access project, funded by US National Science Foundation grant NSF-OPP 0326805. Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author and do not necessarily reflect the views of the National Science Foundation. See  <a href="credits.html" target="new">About this Site</a>  for additional information. 
</p>		
		
<p >
Materials on this site are copyrighted by the original authors, the speakers whose voices are recorded, and the Alaska
Native Language Center. 
There may be additional copyright holders as well as others who
have provided funding or support for work presented here.
Materials may be used freely for non-commercial, educational purposes as specified in the <a href="license.html">license
agreement</a>. Alaska Native Language Archive materials made available through the Dena'ina Qenaga Digital Archive may be subject to more
restrictive conditions of use as specified by the original depositors.
</p>
<p  >
Dena'ina Qenaga website copyright &copy; 2004-<cfoutput>#Year(NOW())#</cfoutput>. Suggestions for future development are welcome. If you have questions or comments about this site, please contact
the DATA Project: denaina [dot] qenaga [at] gmail [dot] com
</p>
	</div>
	
</body>
</html>

