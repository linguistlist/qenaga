<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!---

MAKE SURE YOU KEEP TRACK OF All CHANGES!

Author: Sadie Williams
Date: September 2004
Purpose:

Modifications:
Editor							DD-MMM-YY	Watcha did...
<!--- --- ---- ------- ----- --- -	- --- ---	--- --- --- --->
Edited to work with new DB - April 2005


Changed "Catalog number" to "Identifier"
changed ANLC address to simpler format
left-aligned filenames
2005-09-20 gmh

add a BACK button, 10/9/05 gmh

change file access to pass DigitalFileID via form rather than URL

 --->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="../qenaga.css" />
<title>Dena'ina Language Archive: Display Record</title>

<!-- path to files at ARSC as of April 2017 -->
<cfset filePath = "https://uafanlc.alaska.edu/Online/">

</head>

<!--
clean and set variables sent from browse-resource1.cfm.
-->

<cfif isDefined("ResourceID") and isNumeric(ResourceID)>
	<cfset RESOURCEID = #Trim(ResourceID)#>
<cfelse>
	<cfset RESOURCEID = "">
</cfif>



<cfset DigFileName = "">
<body>
<div id="banner-archive">
	<div id="nav">
		<ul>
			<li><a href="search-1.cfm">Search</a></li>
			<li><a href="browse-1.cfm?format=print" >Documents</a></li>
			<li><a href="browse-1.cfm?format=audio" >Recordings</a></li>
			<li><a href="browse-1.cfm" >Browse</a></li>
			<li><a href="/archive/">Archive</a></li>
			<li><a href="/">Qenaga Home</a></li>
		</ul>
	</div>
</div>
<!--- check to see if the resourceID is defined - if it is, than display the record,
if not, send the user to the index page. --->
<cfif RESOURCEID NEQ "">
<!---
This query gets all of the other resource bundle info to display
 associated with the resource bundle id that was sent through the form
 --->
	<cfquery name="GetResource" datasource="emeld">
SELECT
	DR.RESOURCEID,
	DR.TITLE,
	DR.DESCRIPTION,
	DR.DIALECT,
	DR.DISCOURSETYPE,
	DR.LINGTYPE,
	DR.SOURCE,
	DR.IDENTIFIER,
	DR.IDENTIFIER2,
	DR.PUBLISHER,
	DR.LENGTH,
	DR.FORMAT,
	DR.MEDIATYPE,
	DR.RESOURCELANGUAGE1,
	DR.RESOURCELANGUAGE2,
	DR.DATECREATED
FROM
	DATA_RESOURCE DR
WHERE
	DR.RESOURCEID = #RESOURCEID#
	</cfquery>
<!--- Clean up the description when it's pulled out of the DB --->
	 <cfset DESCRIPTION2 = Replace(#GetResource.DESCRIPTION#,"/P/","<p>","all")>
	 <cfset DESCRIPTION3 = Replace(#DESCRIPTION2#,"/L/","<br>","all")>
<!---
This query pulls out all digital files associated with the resource  id
that was sent through the form.
--->
	<cfquery name="GetDigital" datasource="emeld">
SELECT
	ddf.RESOURCEID,
	ddf.DIGITALFILEID,
	ddf.DFFORMAT,
	ddf.FILENAME,
	ddf.ACCESSIBILITY
FROM
	DATA_DIGITAL_FILE ddf
WHERE
	ddf.RESOURCEID = #RESOURCEID#
ORDER BY
  ddf.DIGITALFILEID
	</cfquery>
	<!---
	This query pulls out all people associated with the resource id
	--->
	<cfquery name="getPeople" datasource="emeld">
SELECT
	dp.DATAPERSONLN,
	dp.DATAPERSONFN,
	dp.DATAPERSONID,
	drp.DATAPERSONID,
	drp.DATAPERSONROLE
FROM
	DATA_PERSON dp,
	DATA_RESOURCE_PERSON drp
WHERE
	dp.DATAPERSONID = drp.DATAPERSONID
	AND
	drp.RESOURCEID = #RESOURCEID#
	</cfquery>

<cfoutput>
<cfif GetResource.recordcount gt 0><!--- If the query returned results, a correct resourceid was sent, (else display an error)  --->
<div id="fullpage">
	<table width="600" align="center">
		<tr>
			<td colspan="2" align="center"><h2>#GETRESOURCE.TITLE#</h2></td>
		</tr>
		<tr class="shade">
			<td width="150"><b>Identifier:</b></td>
			<td><cfif trim(#GETRESOURCE.IDENTIFIER#) neq ''>
					#GETRESOURCE.IDENTIFIER#
					<cfelse>
						No Identifier Defined
					</cfif>
			</td>
		</tr>
		<tr class="shade">
			<td valign="top"><b>Contributor(s):</b></td>
			<td>
			<cfloop query="getPeople">#DATAPERSONLN#, #DATAPERSONFN# (<i>#DATAPERSONROLE#</i>)
			<cfif GetPeople.recordcount gt 1><br></cfif>
			</cfloop></td>
		</tr>
		<tr class="shade">
			<td><b>Date:</b></td>
			<td>
			<cfif trim(#GETRESOURCE.DATECREATED#) neq ''>
				#GetRESOURCE.DATECREATED#
			<cfelse>
			<i>not specified</i>
			</cfif>
			</td>
		</tr>
		<cfif trim(#GETRESOURCE.dialect#) neq ''>
		<tr class="shade">
			<td><b>Dialect:</b></td><td>#GETRESOURCE.dialect#</td>
		</tr>
			</cfif>
		<tr class="shade">
			<td valign="top"><b>Description:</b></td><td>#DESCRIPTION3#</td>
		</tr>
		<cfif trim(#GETRESOURCE.lingtype#) neq ''>
		<tr class="shade">
			<td><b>Content Type:</b></td><td>#GETRESOURCE.lingtype#</td>
		</tr>
			</cfif>
		<cfif trim(#GETRESOURCE.FORMAT#) neq ''>
		<tr class="shade">
			<td><b>Format:</b></td><td>#GETRESOURCE.FORMAT#</td>
		</tr>
		</cfif>
		<cfif trim(#GETRESOURCE.mediatype#) neq ''>
		<tr class="shade">
			<td><b>Media Type:</b></td><td>#GETRESOURCE.mediatype#</td>
		</tr>
		</cfif>
		<cfif trim(#GETRESOURCE.LENGTH#) neq ''>
		<tr class="shade">
			<td><b>Pages:</b></td><td>#GETRESOURCE.LENGTH#</td>
		</tr>
				</cfif>

    <cfset myIdentifier = REREplace(#GETRESOURCE.IDENTIFIER# ,"TI([0-9][0-9][0-9][0-9])","ANLC\1")>
    <cfset myURL = "http://www.uaf.edu/anla/item.xml?id=" & myIdentifier>


		<tr><td colspan="2">&nbsp;</td></tr>
<!--- write out file names if they exists --->
		<cfif trim(#GetDigital.FILENAME#) neq ''>
				<tr>
			<td colspan="2">
				<table width="590"><!--embedded table -->
					<tr>
						<td>&nbsp;&nbsp;</td>
					</tr>
					<tr align="left">
						<td><b>ID</b></td>
						<td><b>Filename</b></td>
						<td><b>Format</b></td>
						<td align="right"><b>Access</b></td>
					</tr>

				<cfset noAccess = 0> <!--- flag for accessibility = "no", used to generate access msg --->
				<cfloop query="getdigital">

				<!--- diplay access buttons (which call terms.cfm) for accessible files --->



					<tr align="left" valign="middle" class="shade">
						<td width="50">#GetDigital.DIGITALFILEID#</td>
            <!-- replace whitespace in filename -->
				<cfset digName = REreplace(#GetDigital.FILENAME#,"%20", " ","ALL")>

					<!-- only write URL if accessible -->
					<!--- allow access for user "audio" to files marked "maybe" --->
				<cfset access = trim(#GetDigital.ACCESSIBILITY#)>
				<cfif #access# eq 'ext'>
						<!--- external link --->
						<td><a href="#FILENAME#">#GetFileFromPath(FILENAME)#</a> [external]</td>
				<cfelseif #access# eq 'yes' or (#access# eq 'maybe' and GetAuthUser() eq 'audio') >
            <cfset fileURL = filePath & myIdentifier & "/" & #GetDigital.FILENAME# >
						<td><a href="#fileURL#">#DigName#</a></td>
        <cfelse>
            <td>#DigName#</td>
				</cfif>
						<td width="50">#GetDigital.DFFORMAT#</td>
						<td align="right" width="50">
              <!-- changed to just display access instead of button, 2017-04-09, gmh --><cfswitch expression="#access#">
                  <cfcase value="yes">open</cfcase>
                  <cfcase value="ext">external</cfcase>
                  <cfcase value="maybe">on&nbsp;request</cfcase>
                  <cfdefaultcase>restricted</cfdefaultcase>
              </cfswitch>

            <!--
						<cfif #access# eq 'yes' or (#access# eq 'maybe' and GetAuthUser() eq 'audio') >
						  <form name="showDigital#GetDigital.DIGITALFILEID#" action="terms.cfm" method="post">
						    <input type="hidden" name="ID" value="#GetDigital.DIGITALFILEID#">
						    <input type="submit" value="view" >
						  </form>
						<cfelseif #access# eq 'ext'>
						  <input type="button" onClick="window.location='#FILENAME#';" value="view">
						<cfelse>#access#
							<cfset noAccess=1>
						</cfif>
          -->

						</td>
					</tr>
					</cfloop>
		</table><!---close embedded table --->
		</td>
	</tr>

		</cfif>
</table>

<!-- if no access display info about ANLA, but may not need this if we point users to ANLA anyway -->
<!--
<cfif  GetDigital.RecordCount gt 0 and noAccess eq 1>
			<p align="center">
			<b>To inquire about obtaining access to the associated digital files please
			 contact ANLA:</b>
			<br>
			<a href="http://www.uaf.edu/anlc">www.uaf.edu/anla</a>
				&nbsp;&nbsp;
				anla@alaska.edu
				&nbsp;&nbsp;
				(907) 474-7436
			</p>
</cfif>
-->

<p align="center">[<a href="#myURL#" target="_new">view this resource in the Alaska Native Language Archive</a>]</p>

<p align="center">[<a href="javascript:history.back();">back</a>]</p>
<br><br>

</div>
<cfelse><!--- the getresource query returned no records, an invalid resourceid --->
<div id="fullpage">
<p>An incorrect resource id been specified.  Please return to the <a href="index.cfm">Dena'ina Digital Archive index page</a> to browse or search for a record to view.</p>

<br><br><br><br><br><br><br><br>
	</div>
</cfif>
</cfoutput>
<!--- If no resourceid was specified, inform the user that they must choose a record to view. --->
<cfelse>
<div id="fullpage">
<p>The record has not been specified or an incorrect record id has been specified.  Please return to the <a href="index.cfm">Dena'ina Digital Archive index page</a> to browse or search for a record to view.</p>

<br><br><br><br><br><br><br><br>
	</div>
	</cfif>
<table width="90%" align="center">
	<tr><td><br /><hr /></td></tr>
	<tr>
		<td align="justify"><p class="fineprint">
Materials on this site are copyrighted by the original authors, the speakers whose voices are recorded, and the Alaska
Native Language Archive. Materials may be used freely for non-commercial, educational purposes as specified in the <a href="../license.html">license
agreement</a>. Alaska Native Language Archive materials made available through the Dena'ina Qenaga Digital Archive may be subject to more
restrictive conditions of use as specified by the original depositors.
</p>
<p  class="fineprint">
Dena'ina Qenaga website copyright &copy; 2004-<cfoutput>#year(now())#</cfoutput>. Suggestions for future development are welcome. If you have questions or comments about this site, please contact
the DATA Project: denaina [dot] qenaga [at] gmail [dot] com.</p>
		</td>
	</tr>
</table>
</body>
</html>
