<!---

displays results of search or redirects to particular browse-2.cfm
instance is search returns unique result

modified by gmh 9/30/05

--->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="../qenaga.css" />
<title>Dena'ina Language Archive -- Search Results</title>
</head>

<!-- clean and set variables sent from search-1.cfm -->    
<cfif IsDefined("form.Title")>
	<cfset TITLE = #trim(form.Title)#>
<cfelse>
	<cfset TITLE = "">
</cfif>
<cfif IsDefined("form.Dialect")>
	<cfset DIALECT = #trim(form.Dialect)#>
<cfelse>
	<cfset DIALECT = "">
</cfif>
<cfif IsDefined("form.LingType")>
	<cfset LINGTYPE = #trim(form.LingType)#>
<cfelse>
	<cfset LINGTYPE = "">
</cfif>
<cfif IsDefined("form.Person")>
	<cfset PERSONID = #trim(form.Person)#>
<cfelse>
	<cfset PERSONID = "">
</cfif>
<cfif IsDefined("form.Keyword")>
	<cfset Keyword = UCase(#trim(form.Keyword)#)>
<cfelse>
	<cfset KEYWORD = "">
</cfif>
<cfif IsDefined("form.DESCRIPTION")>
	<cfset DESCRIPTION = #trim(form.DESCRIPTION)#>
<cfelse>
	<cfset DESCRIPTION = "">
</cfif>
<cfif IsDefined("form.format")>
	<cfset FORMAT = #trim(form.format)#>
<cfelse>
	<cfset FORMAT = "">
</cfif>
<cfif IsDefined("form.digital")>
	<cfset DIGITAL = #trim(form.digital)#>
<cfelse>
	<cfset DIGITAL = "">
</cfif>
<cfif IsDefined("form.StartDate")>
	<cfset DATE1 = #trim(form.StartDate)#>
<cfelse>
	<cfset DATE1 = "">
</cfif>
<cfif IsDefined("form.EndDate")>
	<cfset DATE2 = #trim(form.EndDate)#>
<cfelse>
	<cfset DATE2 = "">
</cfif>
<!--- clean up dates --->
<cfif date1 eq "" and date2 neq "">
	<cfset DATE1 = DATE2>
<cfelseif date1 neq "" and date2 eq "">
	<cfset DATE2 = DATE1>
</cfif>

<!--- searchtype should default to quick search --->
<cfif IsDefined("form.searchtype") AND searchtype eq 1>
<!--- do fielded search --->
<cfquery datasource="Emeld" name="search">
SELECT 
DR.IDENTIFIER ID,
DR.RESOURCEID resid,
DR.TITLE,
DR.DATECREATED,
DR.FORMAT
FROM 
DATA_RESOURCE DR 
WHERE 0=0
<cfif #DATE1# neq "">
AND 
substr(trim(datecreated),1,4) >=  <cfqueryparam value="#TRIM(DATE1)#" CFSQLType="CF_SQL_STRING">
AND 
substr(trim(datecreated),1,4) <= <cfqueryparam value="#TRIM(DATE2)#" CFSQLType="CF_SQL_STRING">
</cfif>
<cfif #TITLE# neq "">
AND  trim(upper(dr.TITLE)) LIKE <cfqueryparam value="%#trim(UCase(TITLE))#%" CFSQLType="CF_SQL_string">
</cfif>
<cfif #FORMAT# neq "">
AND  trim(upper(dr.FORMAT)) LIKE <cfqueryparam value="%#trim(UCase(FORMAT))#%" CFSQLType="CF_SQL_STRING">
</cfif>
<cfif #DESCRIPTION# neq "">
AND trim(upper(dr.DESCRIPTION)) LIKE <cfqueryparam value="%#trim(UCase(DESCRIPTION))#%" CFSQLType="CF_SQL_STRING">
</cfif>
<cfif #DIALECT# neq "">
 AND trim(upper(dr.DIALECT)) = <cfqueryparam value="#trim(UCase(DIALECT))#" CFSQLType="CF_SQL_STRING">
</cfif>
<cfif #LINGTYPE# neq "">
 AND trim(upper(dr.LINGTYPE)) LIKE <cfqueryparam value="%#trim(UCase(LINGTYPE))#%" CFSQLType="CF_SQL_STRING">
</cfif>
<cfif #PERSONID# neq "">
AND DR.RESOURCEID IN (SELECT RESOURCEID FROM DATA_RESOURCE_PERSON WHERE DATAPERSONID = <cfqueryparam value="#PERSONID#" CFSQLType="CF_SQL_INTEGER">) 
</cfif>
<cfif #DIGITAL# eq 1>
AND DR.RESOURCEID IN (SELECT RESOURCEID FROM DATA_DIGITAL_FILE) 
</cfif>
ORDER BY DR.IDENTIFIER
</cfquery>

<cfelse>
<!--- otherwise treat as quicksearch --->

<!--- first break keyword string into separate words --->

<!--- define keywords as a list of keywords --->
<cfset keywords = REReplace(trim(keyword),"\s+",",","ALL")>

<cfquery datasource="Emeld" name="search">
SELECT 
DR.IDENTIFIER ID,
DR.RESOURCEID resid,
DR.TITLE,
DR.FORMAT,
DR.DATECREATED 
FROM 
DATA_RESOURCE DR 
WHERE  ( 0=0)
<cfloop index="key" list="#keywords#">
 AND 
( trim(upper(dr.TITLE)) LIKE <cfqueryparam value="%#KEY#%" CFSQLType="CF_SQL_STRING">) OR ( trim(upper(dr.DESCRIPTION)) LIKE <cfqueryparam value="%#KEY#%" CFSQLType="CF_SQL_STRING">) OR ( dr.RESOURCEID IN (SELECT DRP.RESOURCEID FROM DATA_RESOURCE_PERSON DRP, DATA_PERSON DP WHERE ( DRP.DATAPERSONID = DP.DATAPERSONID ) AND ( (trim(upper(DP.DATAPERSONLN)) LIKE <cfqueryparam value="%#KEY#%" CFSQLType="CF_SQL_STRING">) OR (trim(upper(DP.DATAPERSONFN)) LIKE <cfqueryparam value="%#KEY#%" CFSQLType="CF_SQL_STRING"> )) ) ) 
</cfloop>
 ORDER BY DR.IDENTIFIER
</cfquery>

</cfif>
	
<!--- --->
<cfset records = search.recordcount>


<!--- now build page --->
<body>
<!-- Header -->
<div id="banner-archive">
	<div id="nav">
		<ul>
			<li><a href="search-1.cfm">Search</a></li>
			<li><a href="browse-1.cfm?format=print" >Documents</a></li>
			<li><a href="browse-1.cfm?format=audio" >Recordings</a></li>
			<li><a href="browse-1.cfm" >Browse</a></li>
			<li><a href="/archive/">Archive</a></li>	
			<li><a href="/">Qenaga Home</a></li>
		</ul>
	</div>
</div>
<div id="fullpage">

<h1 align="center">Search Results</h1>

<cfif records eq 1>
	<!--- redirect to record display --->
	<cflocation url = 'browse-2.cfm?ResourceID=#search.resid#'>
<cfelseif records eq 0>
	<h3 align="center">No records returned</h3>
	<p align="center">Try <a href="search-1.cfm">searching again</a> with fewer search criteria.</p>
	<cfelseif records eq 1>
	<h3 align="center">No search criteria specified</h3>
	<p align="center">Try <a href="search-1.cfm">searching again</a> with fewer search criteria.</p>
<cfelse>
	<div align="center" class="instr"><cfoutput>#records#</cfoutput> records returned</div>
</cfif>
<br/><br/>

<cfset row = 0>
<cfoutput query="search">
	 <table  width="600" border="0" cellpadding="0" cellspacing="3" align="center">
		<tbody valign="top" class="shade">
		<tr>
		<td width="50"><b>Identifier:</b></td>
		<td>#ID#
		(#FORMAT#) &nbsp;&nbsp;
		[<a href="browse-2.cfm?ResourceID=#trim(resid)#">view full record</a>]
		
<!--- check for associated digital record --->
<!--- This is a test implementation, gmh 10/19/05 --->
<cfquery name="checkdigital" datasource="emeld">
SELECT DIGITALFILEID 
FROM 
DATA_DIGITAL_FILE
WHERE 
RESOURCEID = <cfqueryparam value="#RESID#" CFSQLType="CF_SQL_INTEGER">
</cfquery>
<cfif checkdigital.recordcount gt 0> &nbsp;<img src="images/cd-1.gif"> </cfif>

		</td>
		</tr>
		<td><b>Title:</b></td>
		<td>#TITLE#</td>
		<tr>
		<tr>
		<td><b>Date:</b></td>
		<td>#DATECREATED#</td>
		</tr>
		<tr>
		<td><b>Participants:</b></td>
<!--- get all persons associated with this resource --->
<cfquery name="getPerson" datasource="Emeld">
SELECT 	DP.DATAPERSONLN,
		DP.DATAPERSONFN,
		DRP.DATAPERSONROLE
FROM
	DATA_PERSON DP,
	DATA_RESOURCE_PERSON DRP
WHERE
	DRP.DATAPERSONID = DP.DATAPERSONID 
	AND
	DRP.RESOURCEID = <cfqueryparam value="#RESID#" CFSQLType="CF_SQL_INTEGER">
ORDER BY
	DRP.DATAPERSONROLE
</cfquery>
 <cfif getPerson.Recordcount gt 0>  
		<td align="left">
		<cfset people=0>
		<cfloop query="getPerson"><cfif people gt 0>; </cfif>#DATAPERSONFN# #DATAPERSONLN#<cfset people=people+1></cfloop>
		</td>
<cfelse>
		<td>&nbsp;&nbsp;</td>
</cfif>
		</tr>
		</tbody>	 
	</table>  <!--- end brief record entry table --->
<br>
	
</cfoutput>

<br><hr width="50%">
<p align="center">[<a href="search-1.cfm">New Search</a>]</p>	

</div>
<table width="90%" align="center">
	<tr><td><br /><hr /></td></tr>
	<tr>
		<td align="justify"><p class="fineprint">
Materials on this site are copyrighted by the original authors, the speakers whose voices are recorded, and the Alaska
Native Language Archive. Materials may be used freely for non-commercial, educational purposes as specified in the <a href="../license.html">license
agreement</a>. Alaska Native Language Archive materials made available through the Dena'ina Qenaga Digital Archive may be subject to more
restrictive conditions of use as specified by the original depositors.
</p>
<p  class="fineprint">
Dena'ina Qenaga website copyright &copy; 2004-<cfoutput>#Year(NOW())#</cfoutput>. Suggestions for future development are welcome. If you have questions or comments about this site, please contact
the DATA Project: denaina [dot] qenaga [at] gmail [dot] com
</p>
		</td>
	</tr>
</table>
</body>
</html>

