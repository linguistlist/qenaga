<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!---
MAKE SURE YOU KEEP TRACK OF All CHANGES!

Author: Sadie Williams
Date: September 2004
Purpose: Searches DATA resources before giving option to Add or Update
Modifications:
Editor							DD-MMM-YY	Watcha did...

gmh	9/23/05   changed display of participants and roles
			standardized display text

changed to order display by IDENTIFIER rather than TITLE   10/3/05 gmh

changed to include browse by format, 10/9/05 gmh

fixed NEXT/PREV buttons to include format= in URL parameters
(letter paramters should go there as well if browse-letter is implemented again)
--  10/9/05 gmh

fixed person query to not display multiple instances of persons with multiple roles
since role is not being displayed on this page
--  10/9/05 gmh

Commented out letter isDefined

--->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../qenaga.css" />
<title>Dena'ina Qenaga Digital Archive: Browse Records</title>
</head>
<!--- <cfif IsDefined("url.letter")>
	<cfset search = #Trim(url.letter)#>
<cfelse>
	<cfset search = "">
</cfif> --->
<cfif IsDefined("url.format")>
	<cfset myFormat = #trim(url.format)#>
<cfelse>
	<cfset myFormat = "all">	
</cfif>

<!--- 
		The following 3 declarations are required the "next" and "previous" buttons.
 --->
 <!--- 
		Maxrows is the number of search results that will be displayed per page. 			
 --->
<cfset MaxRows = 100>
<cfparam name="start" default="1">
<cfparam name="end" default="#MaxRows#">
<body>
<div id="banner-archive">
	<div id="nav">
		<ul>
			<li><a href="browse-print-1.html" >Documents</a></li>
			<li><a href="browse-audio-1.html" >Recordings</a></li>
			<li><a href="browse-all-1.html" >Browse</a></li>
			<li><a href="/archive/">Archive</a></li>	
			<li><a href="/">Qenaga Home</a></li>
		</ul>
	</div>
</div>
<!-- initialize your variables -->
<cfset charlist = "">
<cfset scharlist = "">
<!-- make some mini lists! -->
<cfset charlist = chr(321)>
<cfset scharlist = "L">
<!-- 
This query pulls out all Bundles in the database and their linked persons
-->
	<cfquery name="GetRes" datasource="emeld">
SELECT
	DR.RESOURCEID resid,
	DR.IDENTIFIER,
	DR.TITLE,
	DR.LINGTYPE,
	DR.DATECREATED,
	DR.FORMAT
FROM
	DATA_RESOURCE DR
	<cfif myFormat eq "print" or myFormat eq "audio">
	WHERE
	DR.FORMAT = n'#myFormat#'
	</cfif>
<!---	<cfif isDefined("search")>
	WHERE
	Upper(DR.TITLE) LIKE Upper(N'#search#%')
	</cfif>   --->
ORDER BY
DR.IDENTIFIER  <!--- translate(upper(DR.TITLE), n'#charlist#', n'#scharlist#')  --->
	</cfquery>
<!--- 
		Use these variables to do the "next" and "previous" records. 
 --->
<cfset NextStart = Start + MaxRows>
<cfset NextEnd = End + MaxRows>
<cfset PrevStart = Start - MaxRows>
<cfset PrevEnd = End - MaxRows>
<cfset count = "">
<cfset count = GetRes.recordcount>


<div id="fullpage">
<cfif count gt 0>
<table width="600" cellspacing="10" align="center">
	<tr>
		<td colspan="3" align="center">
		<!--- display appropriate response --->
		<cfif myFormat eq "audio">
			<h1>Browse Recordings</h1>
		<cfelseif myFormat eq "print">
			<h1>Browse Documents</h1>
		<!--- <cfelseif search neq "">
			<h1>Browse Letter #Ucase(search)#</h1> --->
		<cfelse>
			<h1>Browse Records</h1>
		</cfif>
		</td>
	</tr>
	<tr>
		<td align="center">
			<cfoutput>
			<span class="instr">
				Displaying records #start# to  
				<cfif end le count>
					#end# of #count#<br>
				<cfelse>
					#count# of #count#<br>
				</cfif>
			</span>
			</cfoutput>
		</td>
	</tr>
</table>
<table width="600" cellspacing="10" align="center">
	<tr>
		<td>
			<cfoutput query="GetRes" startrow=#start# maxrows=#MaxRows#>
	 		<table  width="590" border="0" cellpadding="0" cellspacing="3">
				<tbody valign="top" class="shade">
				<tr>
					<td width="50"><b>Identifier:</b></td>
					<td>#IDENTIFIER#
						(#FORMAT#) &nbsp;&nbsp;
			[<a href="resourceid-#trim(resid)#.html">view full record</a>]
<!--- check for associated digital record --->
<!--- This is a test implementation, gmh 10/19/05 --->
<cfquery name="checkdigital" datasource="emeld">
SELECT DIGITALFILEID 
FROM 
DATA_DIGITAL_FILE
WHERE 
RESOURCEID = #RESID#
</cfquery>
<!--- would be nice to have cute graphic instead of "D" --->
<cfif checkdigital.recordcount gt 0> &nbsp;<img src="images/cd-1.gif"> </cfif>

					</td>
				</tr>
				<tr>
					<td><b>Title:</b></td>
					<td>#TITLE#</td>
				<tr>
				<tr>
					<td><b>Date:</b></td>
					<td>#DATECREATED#</td>
				</tr>
				<tr>
					<td><b>Participants:</b></td>
<cfquery name="getPerson" datasource="Emeld">
SELECT 	DISTINCT DRP.DATAPERSONID, 
		DP.DATAPERSONLN,
		DP.DATAPERSONFN,
		DRP.RANK
FROM
	DATA_PERSON DP,
	DATA_RESOURCE_PERSON DRP
WHERE
	DRP.DATAPERSONID = DP.DATAPERSONID 
	AND
	DRP.RESOURCEID = #resid#
ORDER BY
	DRP.RANK
</cfquery>
 <!--- If the query returned something, display it otherwise display nothing.  This is here because without it
 a record without any people won't display at all. --->
 <cfif getPerson.Recordcount gt 0>  
		<td align="left">
		<cfset people=0>
		<cfloop query="getPerson">
			<cfif people gt 0>; </cfif>#DATAPERSONFN# #DATAPERSONLN#<cfset people=people+1>
		</cfloop>
		</td>
<cfelse>
		<td>&nbsp;&nbsp;</td> <!--- no people--->
</cfif>

		</tr>
		</tbody>	 
	</table>  <!--- end brief record entry table --->


			<br>
			</cfoutput>
		</td>
	</tr>
	
<cfoutput>
	<tr align="center">
		<td colspan="3">
	<!---making previous button--->
		<cfif PrevStart gte 1>	
			<a href="browse-1.cfm?start=#PrevStart#&end=#PrevEnd#&format=#myFormat#">Previous</a>&nbsp;
		</cfif>
		<!---making intermediate buttons.  Not if there's only one page--->
		<cfif GetRes.recordcount gt maxrows>		
			<cfset jumpsrange = 7>  
			<cfset distance = jumpsrange * maxrows> <!--- seven links before and after --->
			<cfset predots = ""> <!---for display--->
			<cfset postdots = "">
			<cfset from = "">
			<cfset to = "">
			<cfif start gt distance + 1>
				<cfset from = start - distance>
				<cfset predots = "...">
			<cfelse>
				<cfset from = 1>
			</cfif>
			<cfif start lt GetRes.recordcount - distance - maxrows>
				<cfset to = start + distance>
				<cfset postdots = "...">
			<cfelse>
				<cfset to = GetRes.recordcount>
			</cfif>		
			#predots#&nbsp;		
			<cfset x = 0>
			<cfset x = ((from - 1)/maxrows)> <!---counting--->
			<cfloop index="i" from="#from#" to="#to#" step="#maxrows#">
				<cfset x = x + 1>
				<cfif i NEQ start>
				<a href="browse-1.cfm?start=#Evaluate((x * maxrows) - maxrows + 1)#&end=#Evaluate(x * maxrows)#&format=#myFormat#">#x#</a>
				<cfelse>
				<span class="red">#x#</span>
				</cfif>	
			</cfloop>
			#postdots#					
		</cfif>
<!---Making the 'next' button--->
		<cfif end lt GetRes.RecordCount>
&nbsp;&nbsp;&nbsp;<a href="browse-1.cfm?start=#NextStart#&end=#NextEnd#&format=#myFormat#">Next</a>
		</cfif>		
		</td>
	</tr>
	</cfoutput>
</table>
<!--- Else if no records found --->
<cfelse>
<table width="600" cellspacing="10" align="center">
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td colspan="3"><h1>Browse Records</h1></td>
	</tr>
	<tr><td colspan="3" align="center">There are currently no <cfif myFormat eq "audio">Audio <cfelseif myFormat eq "print">Print <cfelse> </cfif>records in the Archive.</td></tr>
</table>
</cfif>
</div>
<table width="90%" align="center">
	<tr><td><br /><hr /></td></tr>
	<tr>
		<td align="justify"><p class="fineprint">
Materials on this site are copyrighted by the original authors, the speakers whose voices are recorded, and the Alaska
Native Language Archive. Materials may be used freely for non-commercial, educational purposes as specified in the <a href="../license.html">license
agreement</a>. Alaska Native Language Archive materials made available through the Dena'ina Qenaga Digital Archive may be subject to more
restrictive conditions of use as specified by the original depositors.
</p>
<p  class="fineprint">
Dena'ina Qenaga website copyright &copy; 2004-<cfoutput>#Year(NOW())#</cfoutput>. Suggestions for future development are welcome. If you have questions or comments about this site, please contact
the DATA Project: denaina [dot] qenaga [at] gmail [dot] com
</p>
		</td>
	</tr>
</table>
</body>
</html>
