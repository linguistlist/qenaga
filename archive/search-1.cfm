<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="../qenaga.css" />
<title>Dena'ina Qenaga Digital Archive -- Search Records</title>

</head> 
<script language="JavaScript" type="text/javascript">
function checkYear(year) {
regEx = /([0-9][0-9][0-9][0-9])/;
  if (!(regEx.test(year.value))) {
    alert ("Please enter the year in the format YYYY.");		
	return false;
  } 	
}
</script> 
<!--- query to populate person dropdown
this should only retrieve people who are associated with resources --->
<cfquery name="person" datasource="emeld">
 SELECT 
 DATAPERSONID ID, 
 DATAPERSONLN LN, 
 DATAPERSONFN FN
 FROM DATA_PERSON DP
 WHERE DP.DATAPERSONID IN (SELECT DISTINCT DRP.DATAPERSONID FROM DATA_RESOURCE_PERSON DRP) 
 ORDER BY Upper(DATAPERSONLN), Upper(DATAPERSONFN)
</cfquery>     
 <!--- set LingTypes (could also get from query if desired --->
<cfset lingtypes="bibliography,dialects,educational,general,grammar,lexicon,miscellaneous,phonology,placenames,text-ethnographic,text-religious,text-traditional"> 
<cfset dialects="Inland,Upper Inlet,Outer Inlet,Iliamna">
   
<body>
<div id="banner-archive">
	<div id="nav">
		<ul>
			<li><a href="search-1.cfm">Search</a></li>
			<li><a href="browse-1.cfm?format=print" >Documents</a></li>
			<li><a href="browse-1.cfm?format=audio" >Recordings</a></li>
			<li><a href="browse-1.cfm" >Browse</a></li>
			<li><a href="/archive/">Archive</a></li>	
			<li><a href="/">Qenaga Home</a></li>
		</ul>
	</div>
</div>
<div id="fullpage">
<h1 align="center">Search Records</h1>
<cfoutput>
<table width="90%" cellspacing="10" align="center" border="0">
	<tr>
		<td>
			<h3>Quick Search</h3>
			<p>Searches across Title, Description, and Contributor fields</p>
			<form name="quick" action="search-2.cfm" method="post">
				<input name="searchtype" type="hidden" value="0">
			<!-- Begin embedded table -->
			<table width="90%" cellspacing="10" align="center">
				<tr>
					<td>
					<input name="keyword" type="text" size="60" maxlength="500"></td>
					<td nowrap><input type="Submit" name="Submit" value="Search"> 
						&nbsp; 
		  			<input type="reset" value="Reset">
					</td>
				</tr>
			</table><!-- end embedded table -->
<!--- cfselect required="yes" name="searchtype" message="Please enter a keyword to search by."></cfselect --->
			</form>
			<h3>Fielded Search</h3>
	<p>You can search in one or more fields.  
			See <a href="##tips">search tips</a> below  </p>
		<!--- would like to implement a double check for year with an onsubit - not working at this time 11-20-05 --->	
			<form action="search-2.cfm" method="post" name="fielded" preloader="no" onsubmit="return checkYear()">
			<input name="searchtype" type="hidden" value="1">
			<!-- embedded table -->
			<table width="90%" cellspacing="10" align="center">
				<tr>
					<td><b>Title:</b></td>
					<td><input name="Title" type="text" size="60" maxlength="500" tabindex="1" ></td>
				</tr>
				<tr>
					<td><b>Description:</b></td>
					<td><input name="Description" type="text" size="60" maxlength="500" tabindex="1" ></td>
				</tr>
				<tr>
					<td><b>Dialect:</b></td>
					<td><SELECT name="Dialect" size="1">
						<option value="">			</option>
						<cfloop list="#dialects#" index="myDialect">
							<option value="#myDialect#">#myDialect#</option>
						</cfloop>
						</SELECT>
					</td>
				</tr>
				<tr>
					<td><b>Content:</b></td>
					<td><SELECT name="LingType" size="1">
			<option value="">         </option>
			<cfloop list="#lingtypes#" index="myType">
			<option value="#myType#">#myType#</option>
			</cfloop> 
		</SELECT></td>
				</tr>
				<tr>
					<td><b>Person:</b></td>
					<td><SELECT name="Person" size="10">
							<cfloop query="person">
							<option value="#ID#">#LN#, #FN#</option> 
							</cfloop>
					</SELECT>
					</td>
				</tr>
				<tr>
					<td><b>Year:</b> (YYYY)</td>
					<td>
						<table>
							<tr>
								<td><input name="StartDate" type="text" size="8" maxlength="4" tabindex="1" onChange="return checkYear(this)"> -
								</td>
								<td><input name="EndDate" type="text" size="8" maxlength="4" tabindex="1" onChange="return checkYear(this)" >
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<tr>
				<td><b>Format:</b></td>
				<td>
					<input type="radio" name="format" value="print" > Print<br /><input type="radio" name="format" value="audio" > Audio
			</td>
			</tr>	
			<tr>
				<td><b>Digital:</b></td>
				<td>
					<input type="checkbox" name="digital" value="1" >
				Limit to records with associated digital files
			</td>
			</tr>
				
			</table>	<!-- end embedded table -->
  		</cfoutput>
		<p align="center">
			<input type="Submit" name="Submit" value="Search"> 
		&nbsp; 
		  <input type="reset" value="Reset">
		</p>
</form>
		</td>
	</tr>
</table>
<p><a name="tips">Search Tips</a></p>
<p>You can search  using multiple criteria; however, in order to retrieve as many records as possible, make your selections simple.</p>
<ul><li>Use keywords in the Title and Description fields. For example search for Legacy instead of A Tanaina Legacy.
</li>
  <li>Avoid using small words like 'the' or 'and'.  </li>
  <li>If you don't find what you are looking for, try searching again without specifying Dialect or Content Type. </li>
  <li>Try searching for people in both the people field AND the description field.</li>
</ul>
</div>
<table width="90%" align="center">
	<tr><td><br /><hr /></td></tr>
	<tr>
		<td align="justify"><p class="fineprint">
Materials on this site are copyrighted by the original authors, the speakers whose voices are recorded, and the Alaska
Native Language Archive. Materials may be used freely for non-commercial, educational purposes as specified in the <a href="../license.html">license
agreement</a>. Alaska Native Language Archive materials made available through the Dena'ina Qenaga Digital Archive may be subject to more
restrictive conditions of use as specified by the original depositors.
</p>
<p  class="fineprint">
Dena'ina Qenaga website copyright &copy; 2004-<cfoutput>#Year(NOW())#</cfoutput>. Suggestions for future development are welcome. If you have questions or comments about this site, please contact
the DATA Project: denaina [dot] qenaga [at] gmail [dot] com
</p>
		</td>
	</tr>
</table>
</body>
</html>


