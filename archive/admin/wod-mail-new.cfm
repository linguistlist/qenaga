<!--- Created by Sadie Williams November 2005 --->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Add Update Featured Words</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="admin.css" type="text/css"/>
</head>
<!--figures out which word is this week's -->
<cfquery name="getids" datasource="emeld">
select ID from DATA_WORDS 
</cfquery>
<cfset start_day = DateDiff("y","03/01/2006",Now())>
<cfset week = (start_day / 7)>
<cfset week = Round(#week#)>
<cfset L_wordIDs = ValueList(getids.id)>
<cfset takeword = ListGetAt(L_wordIDs, #week#)>
<!--pulls this week's word -->
<cfquery name="getword" datasource="emeld">
select 
ID,
WORD,
GLOSS,
LITERAL,
EXAMPLE,
EXAMPLEGLOSS,
SOURCE,
AUDIO,
DIALECT,
WORDDATE
from DATA_WORDS
where ID=#takeword#
</cfquery>




<cfoutput query="getword">

<cfmail from="denaina.qenaga@gmail.com" to="andrea.berez@gmail.com" subject="Dena'ina Qenaga Featured Word" failto="denaina.qenaga@gmail.com">
There is a new Dena'ina Qenaga Featured Word at Qenaga.org!
--------------------------------------------------------------------------
#word#	'#gloss#'

#example#
#examplegloss#




Please visit http://qenaga.org to hear the word and visit the Featured Word Page and Archive.
</cfmail>
</cfoutput>

</html>
