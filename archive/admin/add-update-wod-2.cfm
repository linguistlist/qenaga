<!--- Created by Sadie Williams November 2005 --->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="admin.css">
<title>Add Update Featured Word 2</title>
</head>
<!--- trim variables sent from add-update-wod-1.cfm --->
<cfif isDefined("ID")>
	<cfset ID = #TRIM(ID)#>
<cfelse>
	<cfset ID = "">
</cfif>
<!--- if type is not defined than send the user back to add-update-wod-1 --->
<cfif isDefined("type")>
	<cfset type = #TRIM(type)#>
<cfelse>
	<cflocation url="add-update-wod-1.cfm">
</cfif>
<cfif isDefined("WORD")>
	<cfset WORD = #TRIM(WORD)#>
<cfelse>
	<cfset Word = "">
</cfif>
<cfif isDefined("GLOSS")>
	<cfset GLOSS = #TRIM(GLOSS)#>
<cfelse>
	<cfset GLOSS = "">
</cfif>
<cfif isDefined("LITERAL")>
	<cfset LITERAL = #TRIM(LITERAL)#>
<cfelse>
	<cfset LITERAL = "">
</cfif>
<cfif isDefined("EXAMPLE")>
	<cfset EXAMPLE = #TRIM(EXAMPLE)#>
<cfelse>
	<cfset EXAMPLE = "">
</cfif>
<cfif isDefined("EXAMPLEGLOSS")>
	<cfset EXAMPLEGLOSS = #TRIM(EXAMPLEGLOSS)#>
<cfelse>
	<cfset EXAMPLEGLOSS = "">
</cfif>
<cfif isDefined("SOURCE")>
	<cfset SOURCE = #TRIM(SOURCE)#>
<cfelse>
	<cfset SOURCE = "">
</cfif>
<cfif isDefined("AUDIO")>
	<cfset AUDIO = #TRIM(AUDIO)#>
<cfelse>
	<cfset AUDIO = "">
</cfif>
<cfif isDefined("DIALECT")>
	<cfset DIALECT = #TRIM(DIALECT)#>
<cfelse>
	<cfset DIALECT = "">
</cfif>
<!--- set date format to SQL format --->
<cfif isDefined("date")>
	<cfset worddate = DateFormat(TRIM(date), 'DD-MMM-YYYY')>
<cfelse>
	<cfset worddate = "">
</cfif>
<body>
<cfif type eq "Update"><!--- If type equals Update, then update the record --->
<cfoutput>
<cfquery name="updatewod" datasource="Emeld">
UPDATE Emeld.DATA_WORDS
SET 
WORD = n'#WORD#',
GLOSS = n'#GLOSS#',
LITERAL = n'#LITERAL#',
AUDIO = n'#AUDIO#',
SOURCE = n'#SOURCE#',
DIALECT = n'#DIALECT#',
EXAMPLE = n'#EXAMPLE#',
EXAMPLEGLOSS = n'#EXAMPLEGLOSS#',
worddate = '#worddate#'
WHERE
ID = #ID#
</cfquery>
</cfoutput>
<!--- set the wodID variable for confirm query at the end of the page --->
<cfset wodID = #ID#>
<center><img src="header.jpg"></center>
<h1 align="center">Add/Update Featured Words</h1>
<table><tr><td>The featured word record has been updated as follows:</td></tr></table>
<cfelseif type eq "Add New"><!--- If type equals Add New, then insert a record --->
<cfoutput>
<cftransaction>
<cfquery datasource="Emeld" name="insWOD">
insert into DATA_Words(
ID,  
WORD,
GLOSS,
LITERAL,
AUDIO, 
SOURCE,
DIALECT, 
EXAMPLE,
EXAMPLEGLOSS,
WORDDATE
)
Values
(Emeld.ID.nextval,
n'#WORD#',
n'#GLOSS#',
n'#LITERAL#',
n'#AUDIO#', 
n'#SOURCE#',
n'#DIALECT#', 
n'#EXAMPLE#',
n'#EXAMPLEGLOSS#',
'#WORDDATE#'
)
</cfquery>
<!--- get the ID of the record inserted in the query above --->
<cfquery name="getwodid" datasource="Emeld">
SELECT Emeld.ID.CURRVAL wodID
FROM DUAL
</cfquery>
<!--- set the id to 'wodID' --->
<cfset wodID = #getwodid.wodID#>
</cftransaction>
</cfoutput>
<center><img src="header.jpg"></center>
<h1 align="center">Add/Update Featured Words</h1>
<table><tr><td>A new featured word has been entered</td></tr></table>
<cfelse>
The type of record (add/update) was not defined.
</cfif>

<!--- Confirm Query --->
<cfquery name="ConfWords" datasource="emeld">
SELECT
ID,
WORD,
GLOSS,
LITERAL,
EXAMPLE,
EXAMPLEGLOSS,
SOURCE,
AUDIO,
DIALECT,
WORDDATE
FROM
DATA_WORDS
WHERE ID = #wodID# 
</cfquery>
<!--- Display Confirmed entry --->
<cfoutput>
<table class="input">
	<tr>
		<td>ID:</td>
		<td>#wodID#</td>
	</tr>
	<tr>
		<td>Word:</td>
		<td>#ConfWords.Word#</td>
	</tr>
	<tr>
		<td>Gloss:</td>
		<td>#ConfWords.Gloss#</td>
	</tr>
	<tr>
		<td>Literal:</td>
		<td>#ConfWords.Literal#</td>
	</tr>
	<tr>
		<td>Source:</td>
		<td>#ConfWords.Source#</td>
	</tr>
	<tr>
		<td>Dialect:</td>
		<td>#ConfWords.Dialect#</td>
	</tr>
	
	<tr>
		<td>Example:</td>
		<td>#ConfWords.Example#</td>
	</tr>
	<tr>
		<td>Example Gloss:</td>
		<td>#ConfWords.ExampleGloss#</td>
	</tr>
	<tr>
		<td>Audio:</td>
		<td>#ConfWords.Audio#</td>
	</tr>
	<tr>
		<td>Date:</td>
		<td>#ConfWords.worddate#</td>
	</tr>
</table>

<!--- links --->
<table align="center">
	<tr><td>[<a href="wod-mail.cfm?ID=#wodID#">Notify Qenaga Listserv of new word</a>]
	<tr><td>[<a href="add-update-wod-1.cfm">Add or Update another Featured Word</a>]</td></tr>
	<tr><td>[<a href="admin.cfm">Return to the Admin index page</a>]</td></tr>
</table>
</cfoutput>
</body>
</html>
