<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!---  
	Created by Sadie Williams September 2005
	
   
   Takes values from add-resource-person.cfm and 
   associates them with a resource by inserting into DATA_RESOURCE_PERSON  
   
--->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="admin.css">
<title>Associate Person with Resource</title>
<SCRIPT LANGUAGE="JavaScript" type="text/javascript">
	function refresh(){
		window.opener.location.reload();
		window.close();
	}
</script>
</head>
<!--- Clean up Variables --->
<cfif isDefined("ResID")>
	<cfset ResID = #Trim(ResID)#>
	<!--- If no resource ID is sent than the user is sent back to browse resource page --->
<cfelse>
	<cflocation url="browse-resource-1.cfm">
</cfif>

<cfif isDefined("PersonID")>
	<cfset PersonID = #Trim(PersonID)#>
<cfelse>
	<cfset PersonID = "">
</cfif>

<cfif isDefined("role")>
	<cfset role = #Trim(role)#>
<cfelse>
	<cfset role = "">
</cfif>

<!-- insert a new resourceperson record -->
<cfquery name="insertpr" dbtype="oracle80" datasource="Emeld">
		insert into Emeld.DATA_RESOURCE_PERSON
		(RESPERSLINKID,
		RESOURCEID,
		DATAPERSONID,
		DATAPERSONROLE,
		RANK)
		values 
		(Emeld.RESPERSLINKID.nextval,
		n'#ResID#',
		n'#PersonID#',
		n'#role#',
		n'#rank#')
		</cfquery>
<body>
<center><img src="header.jpg"></center><br>

<!--- add error checking here to report if unsuccessful --->
<cfif 0 eq 0>
<h1>Person successfully associated with resource</h1>

<cfoutput>
<table>
<tr><td>Resource ID</td><td>#ResID#</td></tr>
<tr><td>Person ID</td><td>#PersonID#</td></tr>
</table>
</cfoutput>

<cfelse>
<!--- add error handling --->
<h1>Person Not Added to Resaource</h1>
<p>There was a problem associating the person to the resource.</p>

</cfif>

<br><br>
<table align="center">
	<tr>
		<td><input type="button" onClick="refresh();" value="Close"></td>
	</tr>
</table>



</body>
</html>
