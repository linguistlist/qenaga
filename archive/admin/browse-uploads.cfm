<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--- select a record to update --->
<!---

Send selected ResourceID to add-update-1.cfm as ResID

This page is a modified version of browse-resource1.cfm used to check the Uploaded Records
--->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="admin.css">
<title>Select Resource to Update</title>
<cfquery name="GetResource" datasource="emeld">
SELECT
	DR.RESOURCEID,
	DR.TITLE,
	DR.Identifier
FROM	
	EMELD.DATA_RESOURCE DR
Where dr.added = '1'
ORDER BY
	DR.IDENTIFIER
</cfquery>


<body>
<center><img src="header.jpg"></center><br>
<cfoutput>

<h1>Select Resource to Update</h1>
<p>
There are Currently #GetResource.recordcount# Records
</p>
<!--- changed to direct to add/update page , gmh 10/16/05 --->
<!--- form method="post" action="browse-resource2.cfm" name="form1" --->

<table class="input">
<cfloop query="GetResource">

<th><a href="add-update-1.cfm?ResID=#trim(ResourceID)#">#ResourceID#</a></th>
<td>#IDENTIFIER#</td>
<td>#TITLE#</td>
</tr>
</cfloop>
<tr>
<td colspan="4" align="center">

&nbsp;&nbsp;&nbsp;&nbsp;

</td>
</tr>
</table>


</cfoutput>
</body>
</html>

