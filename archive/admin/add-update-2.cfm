<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!---
	Created by Sadie Williams  September 2005
	Please document any changes below:
  

This page receives data from add-update-1.cfm and inserts or updates record

Variables: ResID

If ResID="" or undefined, treat as insert.

If Submit = "Save", return to add-update-1.cfm for further editing
  
 --->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="admin.css">
<title>Confirm Resource Add/Update</title>
</head>
<body>
<center><img src="header.jpg"></center><br>


<cfoutput>

<cfset DateStamp = DateFormat(Now(), "DD-MMM-YYYY")>

<!--- query to check for valid ResID --->
<!--- add-update-1.cfm shouldn't ever send an invalid ResID to this page,
	but someone might force one in --->
<cfset valid = 0>
<cfif isdefined("form.ResID") and form.ResID neq "">
	<cfquery name="checkResource" datasource="emeld">
	SELECT 
	RESOURCEID
	FROM 
	DATA_RESOURCE 
	WHERE
	RESOURCEID = #form.ResID#
	</cfquery>
	<cfif #checkResource.recordcount# gt 0>
		<cfset valid = 1>
	</cfif>
<cfelse>
	<cfset valid = 1>
</cfif>

<!--- check for valid ResID and print warning if invalid --->
<cfif valid eq 0>
<h1>Unable to update resource</h1>
<p>#ResID# is not a valid Resource ID</p>
<table class="input">
	<tr>
		<td><a href="browse-resource1.cfm">Browse for another record</a></td>
	</tr>
	<tr>
		<td><a href="admin.cfm">Return to administrative interface</a></td>
	</tr>
</table>

<cfelse>
<!-- resource ID okay, so go ahead -->
<!-- This trims the Resource variables sent from add-update-1.cfm -->
<cfif isDefined("form.Dialect")>
		<CFSET Dialect = #TRIM(form.Dialect)#>
	<CFELSE>
		<CFSET Dialect = "">
</cfif>
<cfif isDefined("form.ResourceLanguage1")>
		<CFSET ResourceLanguage1 = #TRIM(FORM.ResourceLanguage1)#>
	<CFELSE>
		<CFSET ResourceLanguage1 = "">
	</cfif>
<cfif isDefined("form.ResourceLanguage2")>
		<CFSET ResourceLanguage2 = #TRIM(FORM.ResourceLanguage2)#>
	<CFELSE>
		<CFSET ResourceLanguage2 = "">
	</cfif>
<cfif isDefined("form.Source")>
		<CFSET Source = #TRIM(FORM.Source)#>
	<CFELSE>
		<CFSET Source = "">
	</cfif>
	<!--- Replace problematic characters for DB --->
<cfif Source neq "">
	<cfset Source = Trim(#form.Source#)>
	<cfset Adjusted2Source = ReplaceList(#Source#, "#chr(149)#,#chr(151)#,#chr(150)#,#chr(34)#,#chr(145)#,#chr(146)#,#chr(147)#,#chr(148)#","-,--,-,'',',','',''")>
<!--- 2. Set a variable for line break (/L/ for line break -- LL convention) --->
	<cfset badchar = "/L/">
<!--- 3. Set a variable for paragraph break (/P/ for paragraph -- LL convention) --->
	<cfset badchar1 = "/P/">
<!--- 4. Paragraph according to HTML is interpreted as the following: --->
	<cfset newchar = "#chr(10)##chr(13)#">
<!--- 5. Replace all line breaks (#chr(10)# in HTML) by /L/: --->
	<cfset Adjusted3Source =  Replace(#Adjusted2Source#,#chr(10)#,#badchar#,"all")>
<!--- 6. Replace all <p> tags (#chr(10)##chr(13)# in HTML) by /P/: --->
	<cfset Source =  Replace(#Adjusted3Source#,#newchar#,#badchar1#,"all")>
<cfelse>
	<cfset Source = "">
</cfif>

<cfif isDefined("form.Title")>
		<CFSET Title = #TRIM(FORM.Title)#>
	<CFELSE>
		<CFSET Title = "">
	</cfif>
<cfif isDefined("form.identifier")>
		<CFSET Identifier = #TRIM(FORM.Identifier)#>
	<CFELSE>
		<CFSET Identifier = "">
	</cfif>
<cfif isDefined("form.identifier2")>
		<CFSET Identifier2 = #TRIM(FORM.Identifier2)#>
	<CFELSE>
		<CFSET Identifier2 = "">
	</cfif>
<cfif isDefined("form.publisher")>
		<CFSET Publisher = #TRIM(FORM.Publisher)#>
	<CFELSE>
		<CFSET Publisher = "">
	</cfif>
<cfif isDefined("form.length")>
		<CFSET Length = #TRIM(FORM.Length)#>
	<CFELSE>
		<CFSET Length = "">
	</cfif>
<cfif isDefined("form.format")>
		<CFSET Format = #TRIM(FORM.Format)#>
	<CFELSE>
		<CFSET Format = "">
	</cfif>
<cfif isDefined("form.mediatype")>
		<CFSET Mediatype = #TRIM(FORM.Mediatype)#>
	<CFELSE>
		<CFSET Mediatype = "">
	</cfif>
<!--- Replace problematic characters for DB --->
<cfif Description neq "">
	<cfset Description = Trim(#form.Description#)>
	<cfset Adjusted2Description = ReplaceList(#Description#, "#chr(149)#,#chr(151)#,#chr(150)#,#chr(34)#,#chr(145)#,#chr(146)#,#chr(147)#,#chr(148)#","-,--,-,'',',','',''")>
<!--- 2. Set a variable for line break (/L/ for line break -- LL convention) --->
	<cfset badchar = "/L/">
<!--- 3. Set a variable for paragraph break (/P/ for paragraph -- LL convention) --->
	<cfset badchar1 = "/P/">
<!--- 4. Paragraph according to HTML is interpreted as the following: --->
	<cfset newchar = "#chr(10)##chr(13)#">
<!--- 5. Replace all line breaks (#chr(10)# in HTML) by /L/: --->
	<cfset Description =  Replace(#Adjusted2Description#,#chr(10)#,#badchar#,"all")>
<!--- 6. Replace all <p> tags (#chr(10)##chr(13)# in HTML) by /P/: --->
<cfelse>
	<cfset Description = "">
</cfif>
<cfif isDefined("form.lingtype")>
		<CFSET LingType = #TRIM(FORM.LingType)#>
	<CFELSE>
		<CFSET LingType = "">
	</cfif>
<cfif isDefined("form.Discoursetype")>
		<CFSET Discoursetype = #TRIM(FORM.Discoursetype)#>
	<CFELSE>
		<CFSET Discoursetype = "">
	</cfif>
<cfif isDefined("form.Datecreated")>
		<CFSET Datecreated = #TRIM(FORM.Datecreated)#>
	<CFELSE>
		<CFSET Datecreated = "">
	</cfif>
<cfif isDefined("form.Comments")>
		<CFSET Comments = #TRIM(FORM.Comments)#>
	<CFELSE>
		<CFSET Comments = "">
	</cfif>
<cfset UpdateDate = DateFormat (Now(), "DD-MMM-YYYY")>

<!--- done cleaning variables ------>


<!--- If we are updating a record!!!  --->
<cfif ResID neq "">

<!-- Updates record in the DATA_Resource Table -->
<cfquery name="updateresource" datasource="Emeld">
UPDATE Emeld.DATA_RESOURCE
SET SubjLanguage = n'TFN',
DIALECT = n'#Dialect#',
ResourceLanguage1 = n'#ResourceLanguage1#',
ResourceLanguage2 = n'#ResourceLanguage2#',
Source = n'#Source#',
Title = n'#Title#',
Identifier = n'#Identifier#',
Identifier2 = n'#Identifier2#',
Publisher = n'#Publisher#',
Length = n'#Length#',
Format = n'#Format#',
Mediatype = n'#Mediatype#',
Description = n'#Description#',
Discoursetype = n'#Discoursetype#',
Lingtype = n'#Lingtype#',
Datecreated = n'#Datecreated#',
Comments = n'#Comments#',
Updatedate = n'#Updatedate#'
where ResourceID = #ResID#
</cfquery>

<!-- If we are adding a record!! Do an insert query -->
<cfelse>


<cftransaction>
<!-- Insert into the Data Resource Table -->
 <cfquery name="insertrb" datasource="Emeld">
insert into Emeld.DATA_RESOURCE
(RESOURCEID,
SUBJLANGUAGE, 
DIALECT, 
RESOURCELANGUAGE1, 
RESOURCELANGUAGE2, 
SOURCE,
TITLE,
IDENTIFIER,
IDENTIFIER2,
PUBLISHER,
LENGTH,
FORMAT,
MEDIATYPE,
DESCRIPTION,
DISCOURSETYPE,
LINGTYPE,
DATECREATED,
COMMENTS,
DATESTAMP)
values 
(Emeld.ResourceID.nextval,
n'TFN', 
n'#Dialect#', 
n'#ResourceLanguage1#', 
n'#ResourceLanguage2#', 
n'#Source#',
n'#Title#',
n'#Identifier#',
n'#Identifier2#',
n'#Publisher#',
n'#Length#',
n'#Format#',
n'#MediaType#',
n'#Description#',
n'#DiscourseType#',
n'#LingType#',
n'#Datecreated#',
n'#Comments#',
'#DateStamp#')
</cfquery>
<cfquery name="getDataRes" datasource="Emeld">
SELECT Emeld.ResourceID.CURRVAL ResID
FROM DUAL
</cfquery>
</cftransaction>
<cfset ResID = #getDataRes.resID#>

<!--- done with insert --->
</cfif>


<!--- now give feedback --->
<cfif type eq "Save">
			<!--- return to add-update-1.cfm for more editing --->
			<cflocation url="add-update-1.cfm?ResID=#ResID#">

<cfelseif type eq "Done">
	
<!-- If add, do a confirm query -->


<!-- This is the confirm query, to be sure everything was added properly -->

<cfquery name="confirm" datasource="Emeld">
select 
dr.ResourceID,
dr.SubjLanguage,
dr.Dialect,
dr.ResourceLanguage1,
dr.ResourceLanguage2,
dr.Source,
dr.Title,
dr.Identifier,
dr.Identifier2,
dr.Publisher,
dr.Length,
dr.Format,
dr.Mediatype,
dr.Description,
dr.DiscourseType,
dr.LingType,
dr.DateCreated,
dr.Comments,
dr.UpdateDate
from 
Emeld.DATA_RESOURCE dr
where 
dr.ResourceID = #ResID#
</cfquery>
<cfquery name="getPeople" datasource="emeld">
SELECT
dp.DATAPERSONLN,
dp.DATAPERSONFN,
dp.DATAPERSONID,
drp.DATAPERSONID,
drp.DATAPERSONROLE,
drp.RESOURCEID,
drp.RESPERSLINKID,
drp.RANK
FROM
EMELD.DATA_PERSON dp,
EMELD.DATA_RESOURCE_PERSON drp
WHERE
dp.DATAPERSONID = drp.DATAPERSONID
AND
drp.RESOURCEID = #ResID#
ORDER BY
drp.RANK
</cfquery>
<cfquery name="GetDigital" datasource="emeld">
SELECT DISTINCT
ddf.ResourceID,
ddf.DIGITALFILEID,
ddf.DFFORMAT,
ddf.FILENAME, 
DDF.ACCESSIBILITY
FROM
EMELD.DATA_DIGITAL_FILE ddf
WHERE
ddf.ResourceID = #ResID#
</cfquery>
<!-- Displays the confirmed update information -->
<h1>Resource has been 
Added/Updated as follows:</h1>
<table class="input">
		<tr><th>ResourceID:</th><td> #resid#</td></tr>
			<tr><th>Subject Language:</th><td> #confirm.SubjLanguage#</td></tr>
			<tr><th>Dialect:</th><td> #confirm.Dialect#</td></tr>
			<tr><th>Resource Language 1: </th><td>#confirm.ResourceLanguage1#</td></tr>
			<tr><th>Resource Language 2:</th><td> #confirm.ResourceLanguage2#</td></tr>
			<tr><th>Source of the resource:</th><td> #confirm.Source#</td></tr>
			<tr><th>Resource title:</th><td> #confirm.Title#</td></tr>
			<tr><th>Identifier:</th><td> #confirm.Identifier#</td></tr>
			<tr><th>Identifier2:</th><td> #confirm.Identifier2#</td></tr>
			<tr><th>Publisher: </th><td>#confirm.Publisher#</td></tr>
			<tr><th>Length:</th><td> #confirm.Length#</td></tr>
			<tr><th>Format:</th><td> #confirm.Format#</td></tr>
			<tr><th>Media Type:</th><td> #confirm.MediaType#</td></tr>
			<tr><th>Description:</th><td> #confirm.Description#</td></tr>
			<tr><th>Discourse Type:</th><td> #confirm.DiscourseType#</td></tr>
			<tr><th>Linguistic Type:</th><td> #confirm.LingType#</td></tr>
			<tr><th>Date Created:</th><td> #confirm.DateCreated#</td></tr>
			<tr><th>Comments:</th><td> #confirm.Comments#</td></tr>
			<tr><th>Update Date:</th><td> #confirm.UpdateDate#</td></tr>
		<tr><th>People</th>
	<td>
	<cfloop query="getPeople">
#RANK#: #DATAPERSONLN#, #DATAPERSONFN# (#DATAPERSONROLE#)<br>
	</cfloop>
	</td>
	</tr>
	<tr><th>Digital Files</th>
	<td>
	<cfloop query="getDigital">
#DIGITALFILEID#: #FILENAME# - #DFFORMAT# - #ACCESSIBILITY#<br>
	</cfloop>
	</td>
	</tr>
</table>



<table align="center">
	<tr>
		<td><a href="add-update-1.cfm?ResID=#ResID#">Update the Resource</a>
		</td>
	</tr>

	<tr>
		<td><a href="add-update-1.cfm">Add a new resource.</a>
		</td>
	</tr>
	<tr>
		<td><a href="browse-resource1.cfm">Browse for another record to update.</a>
		</td>
	</tr>
	<tr>
		<td><a href="javascript:window.close()">Close the window.</a>
		</td>
	</tr>
</table>

<cfelse>  <!-- type is neither Save nor Add/Update -->

<center><img src="header.jpg"></center><br>
<h1>Resource not added/updated</h1>
<p>
		The type of the record (Add/Update or Save) was not correctly defined
</p>

</cfif><!-- end if type is Save, Add/Update or other -->

</cfif><!-- end valid Res ID  -->

</cfoutput>
</body>
</html>