<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!---  
	Created by Sadie Williams September 2005

Gary Holton made the following changes 10/18/05
	Collapsed BrowseResID and ResID 
	Eliminated type variable: ResID="" or undefined treated as add
	Thus, calling page without variable invokes add

--->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add/Update a Resource</title>
<link rel="stylesheet" type="text/css" href="admin.css">
<!-- Begin Javascript -->
<script language="JavaScript" type="text/javascript">
function checkWhitespace(id) {
regEx = /(\s)/;
  if (regEx.test(id.value)) {
    alert ("Identifier contains whitespace - Please enter a valid Identifier.");		
	return false;
  } 	
}
</script>

<!-- If resourceID is sent from an update index, clean them up  -->
<cfif isDefined("ResID")>
	<cfset ResID = #Trim(ResID)#>
	<cfset type="update">
<cfelse>
	<cfset ResID = "">
	<cfset type="add">
</cfif>
<cfif ResID neq "">  <!--- update --->
<!-- query to get resource info to display for update -->
<cfquery name="getResource" datasource="Emeld">
SELECT
dr.RESOURCEID,
dr.SUBJLANGUAGE,
dr.DIALECT,
dr.RESOURCELANGUAGE1,
dr.RESOURCELANGUAGE2,
dr.SOURCE,
dr.TITLE,
dr.IDENTIFIER,
dr.IDENTIFIER2,
dr.PUBLISHER,
dr.LENGTH,
dr.FORMAT,
dr.MEDIATYPE,
dr.DESCRIPTION,
dr.DISCOURSETYPE,
dr.LINGTYPE,			
dr.DATECREATED,
dr.COMMENTS
FROM 
Emeld.DATA_RESOURCE dr
WHERE 
dr.RESOURCEID = #ResID#
</cfquery>
<!--This query pulls out all people associated with the resource  id-->	
<cfquery name="getPeople" datasource="emeld">
SELECT
dp.DATAPERSONLN,
dp.DATAPERSONFN,
dp.DATAPERSONID,
drp.DATAPERSONID,
drp.DATAPERSONROLE,
drp.RESOURCEID,
drp.RESPERSLINKID
FROM
EMELD.DATA_PERSON dp,
EMELD.DATA_RESOURCE_PERSON drp
WHERE
dp.DATAPERSONID = drp.DATAPERSONID
AND
drp.RESOURCEID = #ResID#
</cfquery>
<!-- This query pulls out all digital files associated with ResID -->
<cfquery name="GetDigital" datasource="emeld">
SELECT DISTINCT
ddf.ResourceID,
ddf.DIGITALFILEID,
ddf.DFFORMAT,
ddf.FILENAME, 
DDF.ACCESSIBILITY
FROM
EMELD.DATA_DIGITAL_FILE ddf
WHERE
ddf.ResourceID = #ResID#
</cfquery>


</cfif><!-- end query for Update -->
</head>
<body>
<center><img src="header.jpg"></center>
<br>
<h1 align="center">Add/Update a Resource</h1>
<!--- check for valid ResID and print warning if invalid --->
<cfif #ResID# neq "" and #getResource.recordcount# eq 0>
	<p align="center"><cfoutput>#ResID#</cfoutput> is not a valid Resource ID</p>
	<table class="input" align="center">
		<tr>
			<td><a href="browse-resource1.cfm">Browse for another record</a></td>
		</tr>
		<tr>
			<td><a href="admin.cfm">Return to administrative interface</a></td>
		</tr>
	</table>
<!--- if valid ResID or ResID="" (add) then display the info in a form --->
<cfelse>
<!--- Clean up Source and Description before displaying - removes linebreaks and adds paragraph tags--->
<cfif isDefined("getResource.Source")>
<!--- 2. Set a variable for line break (/L/ for line break -- LL convention) --->
	<cfset badchar = "/L/">
<!--- 3. Set a variable for paragraph break (/P/ for paragraph -- LL convention) --->
	<cfset badchar1 = "/P/">
<!--- 4. Paragraph according to HTML is interpreted as the following: --->
	<cfset newchar = "#chr(10)##chr(13)#">
<!--- 5. Replace all /L/ with line breaks (#chr(10)# in HTML) : --->
	<cfset AdjustedSource =  Replace(#getResource.Source#,#badchar#,#chr(10)#,"all")>
<!--- 6. Replace all /P/ with <p> tags (#chr(10)##chr(13)# in HTML) : --->
<cfset getResource.Source =  Replace(#AdjustedSource#,#badchar1#,#newchar#,"all")>
</cfif><!---  end if for Source Replace--->
<cfif isDefined("getResource.Description")>
<!--- 2. Set a variable for line break (/L/ for line break -- LL convention) --->
	<cfset badchar = "/L/">
<!--- 3. Set a variable for paragraph break (/P/ for paragraph -- LL convention) --->
	<cfset badchar1 = "/P/">
<!--- 4. Paragraph according to HTML is interpreted as the following: --->
	<cfset newchar = "#chr(10)##chr(13)#">
<!--- 5. Replace all /L/ with line breaks (#chr(10)# in HTML) : --->
	<cfset AdjustedDescription =  Replace(#getResource.Description#,#badchar#,#chr(10)#,"all")>
<!--- 6. Replace all /P/ with <p> tags (#chr(10)##chr(13)# in HTML) : --->
<cfset getResource.Description =  Replace(#AdjustedDescription#,#badchar1#,#newchar#,"all")>
</cfif><!---  end if for Description Replace--->
<p><b>*</b> Required field</p>
<cfform name="form1" method="post" action="add-update-2.cfm">
<!-- begin table for form content -->
	<table class="input">
	<cfoutput>
	<!-- ID -->
		<tr><th>Resource ID</th>
			<td>
				<cfif ResID eq "">
					not defined
				<cfelse>
					#ResID#
				</cfif>
			</td>
		</tr>
	<!-- Identifier1 -->
		<tr><th>Identifier *</th>
			<td>
			<cfif isdefined("getResource.Identifier")>
			<cfinput name="Identifier" type="text" size="25" maxlength="30" required="yes" message="Please enter an Identifier" onChange="return checkWhitespace(this)" value="#getResource.Identifier#">
			<cfelse>
			<cfinput name="Identifier" type="text" size="25" maxlength="30" required="yes" message="Please enter an Identifier" onChange="return checkWhitespace(this)">
			</cfif>
			</td>
		</tr>
	<!-- Identifier2 -->
		<tr><th>Secondary Identifier</th>
			<td><input name="Identifier2" type="text" size="25" maxlength="500" <cfif isdefined("getResource.IDENTIFIER2")>value="#getResource.IDENTIFIER2#"</cfif>></td>
		</tr>
	<!-- Title -->
		<tr><th>Title </th>
			<td>
			<input name="Title" type="text" size="60" maxlength="1499" <cfif isdefined("getResource.Title")>value="#HTMLEditFormat(getResource.Title)#"</cfif>>
			</td>
		</tr>
	<!-- Date -->
		<tr><th>Date <span class="instr">YYYY-MM-DD</span></th>
			<td>
			<input name="DateCreated" type="text" size="10" maxlength="10" <cfif isdefined("getResource.DateCreated")>value="#getResource.DateCreated#"</cfif> >
			</td>
		</tr>
	<!-- subject language -->
	<!--- not implemented, all records are TFN --->
	
	<!-- Dialect Table-->
		<tr><th>Dialect</th>
			<td>
			<select name="Dialect" size="1">
			<cfif isdefined("getResource.Dialect")>
			<option value="" <cfif #getResource.Dialect# eq "">selected</cfif>>--- Select ---</option>
			<option value="Upper Inlet" <cfif #getResource.Dialect# eq "Upper Inlet">selected</cfif>>Upper Inlet (Matanuska-Susitna Valley, Tyonek)</option>
			<option value="Outer Inlet" <cfif #getResource.Dialect# eq "Outer Inlet">selected</cfif>>Outer Inlet (Kustatan, Kenai peninsula)</option>
			<option value="Iliamna" <cfif #getResource.Dialect# eq "Iliamna">selected</cfif>>Iliamna (Pedro Bay, Iliamna)</option>
			<option value="Inland" <cfif #getResource.Dialect# eq "Inland">selected</cfif>>Inland (Nondalton, Stony River, Lime Village)</option>
			<cfelse>
				<option value="" selected>--- Select ---</option>
				<option value="Upper Inlet">Upper Inlet (Matanuska-Susitna Valley, Tyonek)</option>
				<option value="Outer Inlet">Outer Inlet (Kustatan, Kenai peninsula)</option>
				<option value="Iliamna">Iliamna (Pedro Bay, Iliamna)</option>
				<option value="Inland">Inland (Nondalton, Stony River, Lime Village)</option>
			</cfif>
			</select>
			</td>
		</tr>
	<!--- We probably don't want all the language options hard-coded here. --->
	<!--ResourceLanguage1 table -->
		<tr>
			<th>Language 1*</th>
			<td>
			<cfselect name="ResourceLanguage1" size="1" message="Please enter a Language of Composition" required="yes">
			<cfif isdefined("getResource.ResourceLanguage1")> 
			<cfset lang = getResource.ResourceLanguage1>
			<option value="ENG" <cfif #lang# eq "ENG">selected</cfif> >English (ENG)</option>
			<option value="TFN" <cfif #lang# eq "TFN">selected</cfif> >Dena'ina (TFN)</option>
			<option value="FRN" <cfif #lang# eq "FRN">selected</cfif> >French (FRN)</option>
			<option value="RUS" <cfif #lang# eq "RUS">selected</cfif> >Russian (RUS)</option>
			<option value="GER" <cfif #lang# eq "GER">selected</cfif> >German (GER)</option>
			<option value="AHT" <cfif #lang# eq "AHT">selected</cfif> >Ahtna (AHT)</option>
			<cfelse>
			<option value="ENG" selected >English (ENG)</option>
			<option value="TFN">Dena'ina (TFN)</option>
			<option value="FRN">French (FRN)</option>
			<option value="RUS">Russian (RUS)</option>
			<option value="GER">German (GER)</option>
			<option value="AHT">Ahtna (AHT)</option>
			</cfif>
			</cfselect>
			</td>
		</tr>
	
	<!--ResourceLanguage2 table -->
		<tr>
			<th>Language 2</th>
			<td>
			<select name="ResourceLanguage2" size="1">
			<cfif isdefined("getResource.ResourceLanguage2")> 
			<cfset lang = getResource.ResourceLanguage2>
			<option value=""  <cfif #lang# eq "">selected</cfif>>--- select ---</option>
			<option value="ENG" <cfif #lang# eq "ENG">selected</cfif>>English (ENG)</option>
			<option value="TFN" <cfif #lang# eq "TFN">selected</cfif>>Dena'ina (TFN)</option>
			<option value="FRN" <cfif #lang# eq "FRN">selected</cfif>>French (FRN)</option>
			<option value="RUS" <cfif #lang# eq "RUS">selected</cfif>>Russian (RUS)</option>
			<option value="GER" <cfif #lang# eq "GER">selected</cfif>>German (GER)</option>
			<option value="AHT" <cfif #lang# eq "AHT">selected</cfif>>Ahtna (AHT)</option>
			<cfelse>
			<option value="" selected>--- select ---</option>
			<option value="ENG"  >English (ENG)</option>
			<option value="TFN">Dena'ina (TFN)</option>
			<option value="FRN">French (FRN)</option>
			<option value="RUS">Russian (RUS)</option>
			<option value="GER">German (GER)</option>
			<option value="AHT">Ahtna (AHT)</option>
			</cfif>
			</select>
			</td>
		</tr>
	<!-- Source-->
		<tr><th>Source</th>
			<td>
			<textarea cols="80" rows="8" name="Source"><cfif isdefined("getResource.Source")>#HTMLEditFormat(getResource.Source)#</cfif></textarea>
			</td>
		</tr>
	<!-- Description -->

		<tr><th>Description</th>
			<td><textarea cols="80" rows="8"  name="Description" maxlength="3999"><cfif isdefined("getResource.Description")>#HTMLEditFormat(getResource.Description)#</cfif></textarea></td>
		</tr>
	<!-- Comments -->
		<tr><th>Comments</th>
		<td><input name="Comments" type="text" size="80" maxlength="500" <cfif isdefined("getResource.Comments")>value="#getResource.Comments#"</cfif> ></td></tr>
			
	 <!-- LingType -->
		<tr><th>Linguistic Type <br><span class="instr">(use ANLC codes)</span></th>
		<td><input name="LingType" type="text" size="60" maxlength="500" <cfif isdefined("getResource.LingType")>value="#getResource.LingType#"</cfif>>
		</td>
		</tr>
	<!-- DiscourseType -->
		<tr><th>Discourse Type</th>
			<td>
			<SELECT name="DiscourseType" size="1">
			
			<cfif isdefined("getResource.DiscourseType")>
			<cfset myDType = getResource.DiscourseType>
			<option value="" <cfif #myDType# eq "">selected</cfif> >--- Select ---</option>
			<option value="drama" <cfif #myDType# eq "drama">selected</cfif>>Drama (plays, skits)</option>
			<option value="formulaic discourse" <cfif #myDType# eq "formulaic discourse">selected</cfif>>Formulaic discourse (prayers, rituals, vows, oaths)</option>
			<option value="interactive discourse" <cfif #myDType# eq "interactive discourse"> selected </cfif>>Interactive discourse (conversations, interviews, correspondance, greetings)</option>
			<option value="language play" <cfif #myDType# eq "language play">selected</cfif>>Language play (verbal art, jokes)</option>
			<option value="oratory" <cfif #myDType# eq "oratory">selected</cfif>>Oratory (sermons, lectures, speeches)</option>
			<option value="narrative" <cfif #myDType# eq "narrative">selected</cfif>>Narrative (stories, myths, folktales)</option>
			<option value="procedural discourse" <cfif #myDType# eq "procedural discourse">selected</cfif>>Procedural discourse (recipes, instructions, plans)</option>
			<option value="report" <cfif #myDType# eq "report">selected</cfif>>Report (essays, news reports, commentaries)</option>
			<option value="singing" <cfif #myDType# eq "singing">selected</cfif>>Singing (chants, songs)</option>
			<option value="unintelligible speech" <cfif #myDType# eq "unintelligible speech">selected</cfif>>Unintelligible speech (sacred language, glossolalia, 'fa-la-la')</option>	
			
			
			<cfelse>
			<option value="" selected>--- Select ---</option>
			<option value="drama" >Drama (plays, skits)</option>
			<option value="formulaic discourse">Formulaic discourse (prayers, rituals, vows, oaths)</option>
			<option value="interactive discourse">Interactive discourse (conversations, interviews, correspondance, greetings)</option>
			<option value="language play">Language play (verbal art, jokes)</option>
			<option value="oratory">Oratory (sermons, lectures, speeches)</option>
			<option value="narrative">Narrative (stories, myths, folktales)</option>
			<option value="procedural discourse">Procedural discourse (recipes, instructions, plans)</option>
			<option value="report">Report (essays, news reports, commentaries)</option>
			<option value="singing">Singing (chants, songs)</option>
			<option value="unintelligible speech" >Unintelligible speech (sacred language, glossolalia, 'fa-la-la')</option>	
			</cfif>
			</SELECT>
			</td>
		</tr>
<!-- Format -->
		<tr><th>Format*</th>
			<td>
			<select name="Format" size="1">	
			<cfif isdefined("getResource.Format")>
			
			<option value="" <cfif trim(#getResource.Format#) eq ""> selected</cfif>>--- Select ---</option>
			<option value="print"
				<cfif trim(#getResource.Format#) eq "print">selected</cfif> > print</option>
			<option  value="audio"
				<cfif #getResource.Format# eq "audio">selected</cfif> > audio</option>
			<option value="video"
				<cfif #getResource.Format# eq "video">selected</cfif> > video</option>
			
			<cfelse>
			
			<option value="">--- Select ---</option>
			<option value="print" > print</option>
			<option  value="audio"> audio</option>
			<option value="video"> video</option>
			
			</cfif>
			</select>
			</td>
		</tr>
	
	<!-- Publisher -->
		<tr><th>Publisher</th>
			<td><input name="Publisher" type="text" size="60" maxlength="500" <cfif isdefined("getResource.Publisher")>value="#getResource.Publisher#"</cfif> >
		</td></tr>
	<!-- Length -->
		<tr><th>Length <span class="instr">(print)</span></th>
			<td><input name="Length" type="text" size="20" maxlength="50" 
			<cfif isdefined("getResource.Length")>value="#getResource.Length#"</cfif>>
			</td>
		</tr>
		
		<!-- MediaType -->
		<tr><th>Media Type<br><span class="instr">audio</span></th>
			<td>
			<SELECT name="MediaType" size="1">
			<option value="">--- Select ---</option>
			
			<option value="cassette" <cfif isdefined("getResource.MediaType")><cfif #getResource.MediaType# eq "cassette"> selected </cfif></cfif>>cassette</option>
			 
			<option value="reel-to-reel" <cfif isdefined("getResource.MediaType")>
			<cfif #getResource.MediaType# eq "reel"> selected </cfif></cfif>>open reel</option>
			
			</SELECT>
			</td>
		</tr> 
	<!--- person --->	
	<cfif type eq "add">
	<!-- If we are adding a record - insert the record before continuing to add people -->
		<tr>
			<th>Person(s)</th>
			<td>
			You must save this record before adding associated person
			</td>
		</tr>
	<cfelse> 
	<!-- display associated persons -->
		<tr>
		<th>Person(s)</th>
			<td>
			<cfloop query="GetPeople">
			#DATAPERSONID#: #DATAPERSONLN#,&nbsp;#DATAPERSONFN# (#DATAPERSONROLE#)
			&nbsp;&nbsp;[<a href="JavaScript:;" onclick="window.open('remove-person.cfm?LinkID=#RESPERSLINKID#','','resizable=yes,status=yes,scrollbars=no,titlebar=yes, width=300,height=300,left=200,screenx=40,top=100,screeny=20');">REMOVE</a>]<br>
			</cfloop>
			</td></tr>
			<tr><td>&nbsp;</td>
			<td><a href="JavaScript:;" onclick="window.open('add-resource-person.cfm?ResID=#ResID#','','resizable=yes,status=yes,scrollbars=yes,titlebar=yes, width=650,height=650,left=200,screenx=40,top=100,screeny=20');">Add a Person</a></td>
		</tr>
	</cfif>
	<!-- Digital Files -->
	<cfif type eq "add">
	<!-- If we are adding a record - insert the record before continuing to add files -->
		<tr>
		<th>Digital Files</th>
			<td>
			You must save this record before adding associated digital files
			</td>
		</tr>
	<cfelse> 
	<!-- display associated files -->
		<tr valign="top"><th>Digital Files</th>
			<td>
			<cfloop query="GetDigital">
			#DIGITALFILEID#: #FILENAME#
			&nbsp;&nbsp;
			[<a href="JavaScript:;" onclick="window.open('delete-digital.cfm?DFID=#DIGITALFILEID#','','resizable=yes,status=yes,scrollbars=yes,titlebar=yes, width=300,height=300,left=200,screenx=40,top=100,screeny=20');">REMOVE</a>]<br>
			</cfloop>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<a href="JavaScript:;" onclick="window.open('add-digital.cfm?ResID=#ResID#','','resizable=yes,status=yes,scrollbars=yes,titlebar=yes, width=650,height=500,left=200,screenx=40,top=100,screeny=20');">Add a Digital File</a></td>
		</tr>
	</cfif> <!-- end if type is add -->
		<tr>
		<td align="center" colspan="2">
		<input type="Submit" name="type" value="Done">
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="Submit" name="type" value="Save">
		
		</td>
		</tr>
		<tr>
		<td align="center" colspan="2">
		(Use <b>Save</b> to enter data and continue editing using this form)
		</td>
		</tr>
	</table>
</cfoutput>
<!-- added cfoutput tags around resID value to properly send variable -->
<cfoutput>
<input type="hidden" name="ResID" value="#ResID#">
</cfoutput>
</cfform>
</cfif> <!--- endif for valid ResourceID --->
	


<br><br><br><br>
<p>Form last updated 25 oct 2005</p>


</body>
</html>
