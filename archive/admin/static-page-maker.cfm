<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--- By Michael Appleby 5 May 2003 
edited by Sarah Murray 03 June 2003, added link to continue working on jobs
Called by editor-approval/staticmaker.cfm.

For each form ID, sends it to the dynamic page and then makes a static version in cf:htdocs.  Where appropriate, it sends it to a special version of the dynamic page with relative links etc.)

--->
<html>
<head>
<title>Static page maker 2</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="admin.css">
</head>
<body>
<center><img src="header.jpg"></center><br>
<cfif IsDefined("from") and IsDefined("to")>
<cfoutput>

<cfset from = trim(from)>
<cfset to = trim(to)>
<cfquery name="checkids" datasource="emeld">
SELECT resourceID from emeld.data_resource where resourceid between #from# and #to#
</cfquery>
<cfset idlist = Valuelist(checkids.resourceID)>
<!--writing each page, printing id's
 -->
 <cfloop list="#idlist#" index="i">

		<cfquery name="selsubid" datasource="emeld">
		SELECT title from emeld.data_resource where resourceid = #i#
		</cfquery>
		<cfset title = #selsubid.title#>
<pre>
title: #title#
resourceid: #i#
------------------------------
</pre>
<cfhttp method="GET" getasbinary="yes" charset="utf-8" url="http://qenaga.org/archive/static-resource.cfm?ResourceID=#i#">
</cfhttp>
	   
<cflock name="MyFileLock" type="EXCLUSIVE" timeout="30">
<cffile action="WRITE" file="/usr/local/apache/qenaga/static/archive/resourceid-#i#.html" 
output="#CFHTTP.FILECONTENT#" attributes="Temporary" mode="777" charset="utf-8" addnewline="Yes" nameconflict="OVERWRITE"> 
</cflock>

Done ResourceID #i#!  <a href="http://qenaga.org/static/archive/resourceid-#i#.html" target="_blank">Check</a><br>
</cfloop>
(ID numbers not mentioned are not in the database and no static page has been made)
</cfoutput>

<br /><br />
 <a href="staticindex.cfm">Click here to make more static pages.</a>

	<cfelse><a href="staticindex.cfm">Go back</a> and enter some ID numbers!
	</cfif>
	
</body>
</html>