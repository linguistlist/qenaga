<!--- Created by Sadie Williams November 2005 --->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Add Update Featured Words</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="admin.css" type="text/css"/>
</head>
<!--- These are possible variables sent - clean up variables, if sent. otherwise set to "" --->
<!--- If type is defined from the first form sent from this page, than trim the variable ("Add New" or "Update")  otherwise set type to "" and records will display --->
<cfif IsDefined("type")>
	<cfset type = trim(#type#)>
<cfelse>
	<cfset type = "">
</cfif>
<!--- if an ID is sent from this page, trim it, else set to "" --->
<cfif IsDefined("ID")>
	<cfset myID = trim(#ID#)>
<cfelse>
	<cfset myID = "">
</cfif>
<!--- This query pulls all words from the database unless we are updating a record, in which case we will pull info for word that we want to update --->
<cfquery name="words" datasource="emeld">
SELECT
ID,
WORD,
GLOSS,
LITERAL,
EXAMPLE,
EXAMPLEGLOSS,
SOURCE,
AUDIO,
DIALECT,
WORDDATE
FROM
DATA_WORDS
<cfif type eq "Update">
WHERE ID = #myID# 
</cfif>
ORDER BY WORDDATE
</cfquery>

<body>
<center><img src="header.jpg"></center>
<h1 align="center">Add/Update Featured Words</h1>
<!--- if type is not specified, than we want to view available records to update --->
<cfif type eq "">
<p class="instr" align="center">Select a word to update or click on "Add New" to add a new word.</p>
<!--- The form gets resent to THIS page using "get" method --->
<form name="selectWord" action="add-update-wod-1.cfm" method="get">
<table align="center" cellpadding=3 class="input">
	<tr>
		<td>Select</td>
		<td>ID</td>
		<td>Date</td>
		<td>Word</td>
		<td>Example</td><br>
		<td>Dialect</td>
		<td>Source/ Filename</td>
	</tr>
<cfoutput>
<!---display available words to update  --->
<cfif words.recordcount gt 0><!--- if there are records in the DB, display --->
<cfloop query="words">
	<tr>
		<td><input type="radio" name="ID" value="#ID#"></td>
		<td>#ID#</td>
		<td><cfif #worddate# NEQ "">
		<!--- Format Worddate, set to 'date'--->
<cfset date = #DateFormat(worddate, 'd mmm yyyy')#>
		#date#<cfelse>--</cfif></td>
		<td><strong><cfif #WORD# NEQ "">#WORD#<cfelse>--</cfif></strong><br>
			<cfif #GLOSS# NEQ "">#GLOSS#<cfelse>--</cfif><BR>
			<cfif #LITERAL# NEQ "">#LITERAL#<cfelse>--</cfif></td>
		<td><b><cfif IsDefined("EXAMPLE")>#EXAMPLE#<cfelse>--</cfif></b>
			<br>
			<cfif IsDefined("EXAMPLEGLOSS")>#EXAMPLEGLOSS#<cfelse>--</cfif></td>
		<td><cfif #DIALECT# NEQ "">#DIALECT#<cfelse>--</cfif></td>
		<td><cfif #SOURCE# NEQ "">#SOURCE#<cfelse>--</cfif><br>
		<cfif #AUDIO# NEQ "">#AUDIO#<cfelse>--</cfif><br>
			</td>
	</tr>
</cfloop>
<cfelse><!--- If there are no records, display message --->
<tr><td colspan="7">There are currently no records in the Featured Word Database</td></tr>
</cfif>
	<tr>
		<td colspan="7" align="center">
			<br>
			<!--- Input button to Update a record --->
			<input type="submit" name="type" value="Update">
			<!--- Reset --->
			<input type="reset" value="Reset">
			<!--- Input button to Add a New record --->
			&nbsp;<input type="submit" name="type" value="Add New"></td>
	</tr>
</table>
</form><!--- end first form --->
</cfoutput>
<cfelse><!--- if type is defined ("Add New" or "Update") then display record for this ID or text boxes if new record. --->
<form name="saveWord" action="add-update-wod-2.cfm" method="post"><!--- This form sends the data input by the user to next page to update or add the record --->
<cfoutput>
<table align="center" class="input">
<tr>
<th>ID:</th>
<td><cfif type neq "Add New">#words.ID#<input type="hidden" name="ID" value="#words.ID#"><cfelse>Not Defined</cfif></td>
</tr>

<tr>
<th>Word:</th>
<td><input type="text" <cfif type neq "Add New">value="#words.word#"</cfif> name="word" size="60" maxlength="200"></td>
</tr>

<tr>
<th>Gloss:</th>
<td><input type="text" name="gloss" <cfif type neq "Add New">value="#words.gloss#"</cfif> size="60" maxlength="200"></td>
</tr>
<tr>
<th>Literal:</th>
<td><input type="text" name="literal" <cfif type neq "Add New">value="#words.literal#"</cfif> size="60" maxlength="200"></td>
</tr>

<tr valign="top">
<th>Example:</th>
<td><input type="text" name="example" size="60" maxlength="400" <cfif type neq "Add New">value="#words.example#"</cfif>></td>
</tr>

<tr valign="top">
<th>Example Gloss:</th>
<td><input type="text" name="examplegloss" size="60" maxlength="400" <cfif type neq "Add New">value="#words.examplegloss#"</cfif>></td>
</tr>

<tr>
<th>Source:</th>
<td><input type="text" name="source" <cfif type neq "Add New">value="#words.source#"</cfif> size="60" maxlength="200"></td>
</tr>
<tr>
<th>Audio:</th>
<td><input type="text" name="audio" <cfif type neq "Add New">value="#words.audio#"</cfif> size="60" maxlength="200"></td>
</tr>
<tr>
<th>Dialect:</th>
<td><input type="text" name="dialect" <cfif type neq "Add New">value="#words.dialect#"</cfif> size="60" maxlength="200"></td>
</tr>
<!--- Format Worddate, set to 'date'--->
<cfif isDefined('words.worddate')>
<cfset date = #DateFormat(words.worddate, 'd mmm yyyy')#>
<cfelse>
<cfset date = "">
</cfif>

<tr>
<th>Date:</th>
<td><input type="text" name="date" <cfif type neq "Add New">value="#date#"</cfif> size="60" maxlength="200"></td>
</tr>
<input type="hidden" name="type" value="#type#">
<tr>
<td colspan="2" align="center">
<input type="submit" value="Submit">
&nbsp;&nbsp;
<input type="reset" value="Reset">
</td></tr>
</table>
</cfoutput>
</form>
<p align="center">[<a href="javascript:history.back();">back</a>]</p>
</cfif>





</body>
</html>
