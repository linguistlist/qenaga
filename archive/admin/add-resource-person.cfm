<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!---  
	Created by Sadie Williams September 2005

   
 This form displays list of current persons and role type
 in order to associate an existing person with a resource.  ResourceID must be specified
 from the add-update-1.cfm page - otherwise user gets an error directing them back to the browse-resource page.
    
   
   
--->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="admin.css">
<title>Associate Person With Resource</title>
</head>
<cfif isDefined("ResID")>
	<cfset ResID = #Trim(ResID)#>
<cfelse>
	<cfset ResID = "">
</cfif>
<cfif ResID neq "">
<!-- query to populate person dropdown -->
<cfquery name="DataPerson" datasource="emeld">
   Select 
   DATAPERSONID,
   DATAPERSONLN, 
   DATAPERSONFN
   From 
   emeld.DATA_PERSON
   ORDER BY Upper(DATAPERSONLN), Upper(DATAPERSONFN)
   </cfquery>   
<!-- create a list to populate personrole select -->
<cfset personrole = "author,editor,speaker,transcriber,translator,interviewer,recorder,contributor"> 

<body>
<center><img src="header.jpg"></center><br>

<cfoutput>
<!-- This is the Data logo header -->

<!-- This is the form for adding/updating a DATA audio resource -->
<!--- no!  this is for ANY resource!!  gmh, 10/16/05 --->
<h1 align="center" >Associate Person With Resource</h1>

<p align="center">
Choose person, role, and rank to associate with resource ID #ResID#
</p>

<cfform name="form1" method="post" action="add-resource-person-2.cfm">

<table class="input"  align="center">
<tr>
	<td>
	<!-- begin embedded table -->
		<table>
			<tr><th>Name:</th>
<td><cfSELECT name="PersonID" size="20" required="yes" message="Please select a person">
	<cfloop query="DataPerson">
	<option value="#DATAPERSONID#">#DATAPERSONLN#, #DATAPERSONFN# 
	</cfloop>		
	</cfSELECT>
	</td>
	</tr>
		</table><!-- end embedded table -->
	</td>
	<td>
	<!-- begin embedded table -->
	<table><tr><th>Role</th>
<td width="300" align="center"><cfSELECT name="role" size="10" required="yes" message="Please select a Role." >
				<cfloop index="name" list="#personrole#" delimiters=",">
				<option value="#name#">#name# 
				</cfloop>		
			</cfSELECT>
	</td>
</tr></table><!-- end embedded table -->
</td></tr>

</table>
	


<table class="input"  align="center">
<tr><th>Rank</th>
<td>
<select name="rank" size="1">
<option value="" selected>---SELECT---</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
</select>
</td></tr>

	<tr><td colspan="2" align="center">
	<input type="Submit" name="Submit" value="Submit" onclick="refresh()">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="Reset" value="Clear Form"></td></tr>

</table>

<input type="hidden" name="ResID" value="#ResID#">

	
</cfform>
</cfoutput>
<cfelse>
Please return to the <a href="browse-resource-1.cfm">Browse Resource Page</a> to specify a Resource to link the person with.  If you would like to add a person to the database visit the <a href="add-person1.cfm">add person page</a>.
</cfif>
<br><br><br><br>
<p>Form last modified 01 nov 2005</p>

</body>
</html>
