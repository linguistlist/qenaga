<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<!---Created 5 May 2003 by Michael Appleby
Adapted for Qenaga.org 4 April 2006 by Andrea Berez

Sends ID numbers to the static page maker  which in turn calls the actual page for each ID number.
--->



<head>
<title>Static page maker</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="admin.css">
</head>

<body>
<center><img src="header.jpg"></center><br>
<table>
	
	
	
	<tr>
		<td colspan="2" align="center"><h1>Create a static page!</h1></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td colspan="2">
		<p>This page generates static versions of dynamic pages in the site, for burning the entire site onto DVD.<br></p>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td colspan="2">
		<p>To create a single static page, enter the same ID number in the 'from' and the 'to' box.<br></p>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="left" width="100%" bgcolor="#F0F0F0" colspan="2">
		<span class="p-match">Archive Pages
		</span>
		</td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
			

	<cfquery name="findresourceidmaxmin" datasource="emeld">
	select max(resourceid) maxresourceid, min(resourceid) minresourceid from emeld.data_resource
	</cfquery>		
	<cfform action="static-page-maker.cfm" method="POST" name="get-static">
		<tr>
			<td align="right" valign="top">
			Archive: Enter the ResourceIDs you wish to make static:&nbsp;&nbsp;&nbsp;
			
			</td>
			<td>From <input type="text" size="5" maxlength="6" name="from"> to <input type="text" size="5" maxlength="6" name="to"> (inclusive)&nbsp;&nbsp;<input type="submit" name="submit" value="&nbsp;Go&nbsp;">
<br>
<cfoutput>(lowest ResourceID is #findresourceidmaxmin.minresourceid#; highest ResourceID is #findresourceidmaxmin.maxresourceid#)</cfoutput>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</cfform>
	
	<!--to make static browse-resource pages -->
	
	<tr>
		<td align="left" width="100%" bgcolor="#F0F0F0" colspan="2">
		<span class="p-match">Browse Resource Pages
		</span>
		</td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="right" valign="top">
			Click the button to make static Browse-Resource pages:
			</td>
			<td>
			<a href="static-browse-maker.cfm">Go!</a>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	

	<!--to make static browse-documents pages -->
	
	<tr>
		<td align="left" width="100%" bgcolor="#F0F0F0" colspan="2">
		<span class="p-match">Browse Documents Pages
		</span>
		</td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="right" valign="top">
			Click to make static Browse-Documents pages:
			</td>
			<td>
			<a href="../static-browse.cfm?format=print">Go!</a>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	


	<!--to make static browse-audio pages -->
	
	<tr>
		<td align="left" width="100%" bgcolor="#F0F0F0" colspan="2">
		<span class="p-match">Browse Audio Pages
		</span>
		</td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="right" valign="top">
			Click the button to make static Browse-Audio pages:
			</td>
			<td>
			<a href="../static-browse.cfm?format=audio">Go!</a>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

	<tr><td>&nbsp;</td></tr>	
	
</table>

</body>
</html>

