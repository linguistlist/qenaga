<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--- Access this page via add-update-1.cfm 

Resource ID sent as ResID

--->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="admin.css">
<title>Add Digital File</title>
</head>
<!---If the Resource Id is defined than clean it --->
<cfif isDefined("ResID")>
<cfset ResID = #Trim(ResID)#>
<cfelse>
<cfset ResID = "">
</cfif>
<!--- If the resID is not null than display the form --->
<cfif ResID neq "">
<!--- Get resource info --->
<cfquery name="GetResource" datasource="emeld">
SELECT
	dr.RESOURCEID,
	dr.TITLE
FROM
	EMELD.DATA_RESOURCE dr
WHERE
	dr.RESOURCEID = #ResID#
</cfquery>
<!--- get formats to populate dropdown --->
<cfquery name="GetFormat" datasource="emeld">
Select
df.Format
from
data_format df
</cfquery>
<body>
<center><img src="header.jpg"></center><br>
<h1 align="center">Add an Associated Digital File to a Resource</h1>
<cfoutput>	

<table border="0"  align="center">
<tr>
<td>Identifier</td><td>#GetResource.ResourceID#</td></tr>
<tr><td>Title:</td><td>#GetResource.Title#</td></tr>
</table>
<br>
<cfform name="form1" action="add-digital-2.cfm" method="post">

<table class="input" align="center">
<tr>
<th>Filename</th>
<td><cfinput name="FN" type="text" size="60" maxlength="500" tabindex="1" required="yes" message="Please input a filename.">
</td>
</tr>
<tr>
<th>Format</th>
<td>
<CFSELECT name="Format" size="1" required="yes" message="Please choose a digital file format.">
				<option value="">--Select--</option>
				<cfloop query="getFormat">
					<option value="#Format#">#Format#</option>
				</cfloop>
		</CFSELECT>
</td>
</tr>

<tr>
<th>Accessibility</th>
<td>
<SELECT name="Access" size="1">
				<option value="maybe" selected>maybe</option>
				<option value="yes">yes</option>
				<option value="no">no</option>
</SELECT>
</td>
</tr>
	
<input name="ResID" type="hidden" value="#ResID#">
<tr>
<td align="center" colspan="2">
<input type="Submit" name="Submit" value="Add">
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="Reset" >
</td>
</tr>
</table>
</cfform>
</cfoutput>
<br><br><br><br>
<p>Form last updated 01 nov 2005</p>
<cfelse>
<body>
<center><img src="header.jpg"></center><br>
<p>No resource ID was sent to this page.  Please return to the <a href="browse-resource-1.cfm">Browse Resource</a> page to specify a resource to link the digital file to.</p>
</cfif>
</body>
</html>
