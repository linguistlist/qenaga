<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!---  
	Created by Sadie Williams on September 9, 2004
	
delete associated digital file from DATA_DIGITAL_FILE
   
--->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="admin.css">
<title>Delete Digital File</title>
<SCRIPT LANGUAGE="JavaScript">
	function refresh(){
		window.opener.location.reload();
		window.close();
	}
</script>
</head>
<cfif isDefined("DFID")>
	<cfset DFID = #Trim(DFID)#>
<cfelse>
	<cfset DFID = "">
</cfif>  
<cfif DFID neq "">
<cfquery name="DeleteDigital" datasource="Emeld">
DELETE
FROM
DATA_DIGITAL_FILE
WHERE
DIGITALFILEID = #DFID#
</cfquery>

<body>

<h1>Delete Digital File</h1>
<p>
Digital file ID <cfoutput>#DFID#</cfoutput> has been removed
</p>

<br><br>
<table align="center">
	<tr>
		<td><input type="button" onClick="javascript:refresh();" value="Close"></td>
	</tr>
</table>

</body>
<cfelse>
<body>You must specify a digital file ID.  Return to the <a href="browse-resource-1.cfm">Browse-Resource Page</a> to choose a resource and digital file.</body>
</cfif>
</html>
