<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="admin.css">
<title>Add/Update an Associated Digital File</title>
<SCRIPT LANGUAGE="JavaScript" type="text/javascript">
<!--
	function refresh()
	{
		window.opener.location.reload();
		window.close();
	}
-->
</script>
</head>
<!--- clean and set variables sent from add-digital.cfm --->
<cfset DateStamp = DateFormat(Now(), "DD-MMM-YYYY")>
<!--- If no ID is sent than the user is sent back to the add-update resource page --->
<cfif isDefined("ResID")>
	<cfset ResID = #Trim(ResID)#>
<cfelse>
	<cflocation url="add-update-1.cfm">
</cfif>
<cfif isDefined("FORMAT")>
	<cfset FORMAT = #Trim(FORMAT)#>
<cfelse>
	<cfset FORMAT = "">
</cfif>
<cfif isDefined("FN")>
	<cfset FN = #Trim(FN)#>
<cfelse>
	<cfset FN = "">
</cfif>
<cfif isDefined("ACCESS")>
	<cfset ACCESS = #Trim(ACCESS)#>
<cfelse>
	<cfset ACCESS = "">
</cfif>

<cftransaction>
<cfquery name="insertdf" datasource="Emeld">
INSERT INTO Emeld.DATA_DIGITAL_FILE
		(DIGITALFILEID,
		DFFORMAT,
		FILENAME,
		ACCESSIBILITY,
		RESOURCEID,
		DATESTAMP)
		values 
		(Emeld.DIGITALFILEID.nextval,
		n'#FORMAT#',
		n'#FN#',
		n'#ACCESS#',
		n'#RESID#',
		'#DATESTAMP#'
		)
</cfquery>
<cfquery name="getCurrVal" datasource="Emeld">
SELECT Emeld.DIGITALFILEID.CURRVAL dfID
FROM DUAL
</cfquery>
</cftransaction>
   
<body>

<!--- add error checking here to report if unsuccessful --->
<cfif 0 eq 0>
<h1 align="center">Digital File Added</h1>

<cfoutput>
<table align="center">
<tr><td>Resource ID</td><td>#ResID#</td></tr>
<tr><td>Digital File ID</td><td>#getCurrVal.dfID#</td></tr>
</table>
</cfoutput>

<cfelse>
<!--- add error handling --->
<h1 align="center">Digital File Not Added</h1>
<p align="center">There was a problem adding the digital file.</p>

</cfif>

<br><br>

<!--- clicking "ADD" takes user back to calling page 
		(ostensibly add-update-1.cfm  and refreshes --->
<table align="center">
	<tr>
		<td><input type="button" onClick="refresh();" value="Close"></td>
	</tr>
</table>



</body>
</html>
