<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!---  
Modifed 16 oct 2005 by Gary Holton
Clarified treatment of ResPersLinkID
Parameter passed to this page is LinkID sent from add-update-1.cfm
--->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="admin.css">
<title>Remove Person</title>
<SCRIPT LANGUAGE="JavaScript">
	function refresh(){
		window.opener.location.reload();
		window.close();
	}
</script>
</head>



<body>
<center><img src="header.jpg"></center><br>


<!--- add error checking here to report if unsuccessful --->
<cfif IsDefined("LinkID")>
<cfquery name="RemovePerson" datasource="Emeld">
DELETE
FROM
DATA_RESOURCE_PERSON
WHERE
RESPERSLINKID = trim(#LinkID#)
</cfquery>

<cfif 0 eq 0>
<!---
it would be nice to be able to check to see that record was actually deleted,
but recordCount is not defined for delete queries
<cfif #RemovePerson.recordcount# gt 0>
--->
	<h1>Person Removed</h1>
	<p>
	This person is no longer associated with the resource.
	</p>
	<cfelse>
	<h1>Person Not Removed</h1>
	<p>There was a problem removing the person.</p>	
	</cfif>
	
<cfelse>

<!--- add error handling --->
<h1>Person Not Removed</h1>


<cfoutput><p>Invalid Link ID: <cfif isDefined("linkid")>#LinkID#<cfelse>Link ID not defined</cfif></p></cfoutput>
<p>You can return to the <a href="browse-resource1.cfm">Browse-Resource</a> page to remove a person from a record. </p>
</cfif>

<br><br>
<table align="center">
	<tr>
		<td><input type="button" onClick="javascript:refresh();" value="Close"></td>
	</tr>
</table>

	
</body>
</html>
