<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--- By Michael Appleby 5 May 2003 
edited by Sarah Murray 03 June 2003, added link to continue working on jobs
Called by editor-approval/staticmaker.cfm.

For each form ID, sends it to the dynamic page and then makes a static version in cf:htdocs.  Where appropriate, it sends it to a special version of the dynamic page with relative links etc.)

--->
<html>
<head>
<title>Static Browse maker 2</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="admin.css">
</head>
<body>
<center><img src="header.jpg"></center><br>
<cfif IsDefined("url.format")>
	<cfset myFormat = #trim(url.format)#>
<cfelse>
	<cfset myFormat = "all">	
</cfif>
<cfoutput>

<!--writing each page, printing id's
 -->

<cfhttp method="GET" getasbinary="yes" charset="utf-8" url="http://qenaga.org/archive/static-browse.cfm?format=#myFormat#">
</cfhttp>
	   
<cflock name="MyFileLock" type="EXCLUSIVE" timeout="30">
<cffile action="WRITE" file="/usr/local/apache/qenaga/static/archive/browse-#myFormat#.html" 
output="#CFHTTP.FILECONTENT#" attributes="Temporary" mode="777" charset="utf-8" addnewline="Yes" nameconflict="OVERWRITE"> 
</cflock>

Done!  <a href="http://qenaga.org/static/archive/browse-#myformat#.html" target="_blank">Check</a><br>
</cfoutput>

<br /><br />
 <a href="staticindex.cfm">Click here to make more static pages.</a>

	
</body>
</html>