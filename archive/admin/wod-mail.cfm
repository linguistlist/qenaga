<!--- Created by Sadie Williams November 2005 --->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Add Update Featured Words</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="admin.css" type="text/css"/>
</head>
<!--- if an ID is sent from this page, trim it, else set to "" --->
<cfif IsDefined("ID")>
	<cfset ID = trim(#ID#)>
<cfelse>
	<cfset ID = ""><!--- need to display a message saying that ID was not specified --->
</cfif>
<!--- The mail variable is sent from the first instance of this page --->
<cfif IsDefined("mail")>
	<cfset mail = trim(#mail#)>
<cfelse>
	<cfset mail = "">
</cfif>
<!--- query to get wod info --->
<cfquery name="getword" datasource="emeld">
SELECT
ID,
WORD,
GLOSS,
LITERAL,
EXAMPLE,
EXAMPLEGLOSS,
SOURCE,
AUDIO,
DIALECT,
WORDDATE
FROM
DATA_WORDS
WHERE ID = #ID# 
</cfquery>

<body>
<center><img src="header.jpg"></center>
<h1 align="center">Add/Update Featured Words</h1>
<!--- if an id is specified and the mail variable was not sent from the first page, display the info --->
<cfif ID neq "" and mail eq "">
<form action="wod-mail.cfm" method="get" name="displaymail">
<!--- display wod email message --->
<p>The email sent to the qenaga listserv will display as follows:</p>
<p>Dena'ina Qenaga Featured Word</p>
<br/>
<cfoutput query="getword">
<input type="hidden" value="#ID#" name="ID">
<p>#word#	'#gloss#'</p>
<br />
<p>#example#</p>
<p>#examplegloss#</p>
<p>#source#</p>
<p>#dialect#</p>

<br />
<p>Please visit <a href="http://qenaga.org">http://qenaga.org</a> to hear the word and visit the Featured Word Archive.</p>
<!--- ask user if this is what they want to send--->
<span class="instr">Click on "Submit" if the information above is what you would like to send to the list.  Otherwise click "Cancel"</span>
<br />
<!--- create a variable mail used to determine which form in this page the user should go to --->
<cfset mail = "yes">
<input type="hidden" name="mail" value="mail">
</cfoutput>
<!--- Input button to Submit form--->
	
			<input type="submit" name="submit" value="Submit">
<!--- Cancel --->
			<input type="button" value="Cancel" onClick="location.href='../archive/admin/add-update-wod-1.cfm">
</form>
<!--- if the id is specified and the mail variable was sent as yes from the first instance of this page, --->
<cfelseif ID neq "" and mail neq "">
<!--- send the email and the confirmation --->
<cfoutput query="getword">

<cfmail from="denaina.qenaga@gmail.com" to="andrea.berez@gmail.com" subject="Dena'ina Qenaga Featured Word" failto="denaina.qenaga@gmail.com">
Dena'ina Qenaga Featured Word
--------------------------------------------------------------------------
#word#	'#gloss#'

#example#
#examplegloss#

#source#
#dialect#


Please visit http://qenaga.org to hear the word and visit the Featured Word Archive.
</cfmail>
</cfoutput>
<!--- confirmation --->

<table align="center">
<tr><td><p>An email has been sent to the Dena'ina Qenaga Listserv.</p></td></tr>
	<tr><td>[<a href="add-update-wod-1.cfm">Add or Update another Featured Word</a>]</td></tr>
	<tr><td>[<a href="admin.cfm">Return to the Admin index page</a>]</td></tr>
</table>


<cfelse><!--- error message --->
The id was not specified.
</cfif>

</body>
</html>
