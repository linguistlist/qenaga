<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="../qenaga.css" />
<title>Dena'ina Qenaga Digital Archive</title>
</head>
<body>
<div id="banner-archive">
	<div id="nav">
		<ul>
			<li><a href="search-1.cfm">Search</a></li>
			<li><a href="browse-1.cfm?format=print" >Documents</a></li>
			<li><a href="browse-1.cfm?format=audio" >Recordings</a></li>
			<li><a href="browse-1.cfm" >Browse</a></li>
			<li><a href="/archive/">Archive</a></li>
			<li><a href="/">Qenaga Home</a></li>
		</ul>
	</div>
</div>
<div id="fullpage">
		<h1 align="center">Welcome to the Dena'ina Qenaga Digital Archive</h1>
<table width="600" align="center">
	<tr >
		<td colspan="2" ><p>The Dena'ina Qenaga Archive provides digital access to more than five hundred documents and recordings relating to the Dena'ina language, including nearly everything written in or about Dena'ina language. You may browse or search the available records.  Some of the records have associated digital audio and text
files which can downloaded.
In some cases, access restrictions prohibit web-based access to digital materials. However, digital copies may still be available directly from the Alaska Native Language Center.
 </p>
		</td>
	</tr>
	<tr><td align="center">
		<table width="200"><tr>
		<td align="center" class="fineprint"><img src="../images/anlc.jpg" alt="ANLC Archive" width="200" height="150"><br></td></tr></table>
</td>
<td>
      <h3><a href="browse-1.cfm">Browse All Records</a></h3>
      <h3><a href="browse-1.cfm?format=print">Browse Documents</a></h3>
      <h3><a href="browse-1.cfm?format=audio">Browse Recordings</a></h3>
      <h3><a href="search-1.cfm">Search</a></h3>
		</td>
	</tr>
</table>
<table width="80%" align="center" style=" background-color: white; border:1px solid #35331C;" >
			<tr>
				<td align="center"><a href="../denaina-bibliography-version-3-1-g.pdf">Download <em>Bibliography of Sources on Dena'ina and Cook Inlet Anthropology, v. 3.1 (2012)</em></a>
				</td>
			</tr>
		</table>
</div>
<table width="90%" align="center">
	<tr><td><br /><hr /></td></tr>
	<tr>
		<td align="justify"><p class="fineprint">
Materials on this site are copyrighted by the original authors, the speakers whose voices are recorded, and the Alaska
Native Language Archive. Materials may be used freely for non-commercial, educational purposes as specified in the <a href="../license.html">license
agreement</a>. Alaska Native Language Archive materials made available through the Dena'ina Qenaga Digital Archive may be subject to more
restrictive conditions of use as specified by the original depositors.
</p>
<p  class="fineprint">
Dena'ina Qenaga website copyright &copy; 2004-<cfoutput>#Year(NOW())#</cfoutput>. Suggestions for future development are welcome. If you have questions or comments about this site, please contact
the DATA Project: denaina [dot] qenaga [at] gmail [dot] com
</p>
		</td>
	</tr>
</table>
</body>
</html>
