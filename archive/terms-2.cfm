<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!---

This page accepts response from terms.cfm and opens the digital file
 --->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="/qenaga.css" />
<title>Terms and Conditions of Use</title>

<!--- 
<cfset pdfpath = "http://www.qenaga.org/archive/docs/">
<cfset audioPath = "http://snowy.arsc.alaska.edu/anlc/records/ANLC">
--->

<!-- this file path should work for both print and audio
	Gary Holton, 2017-03-31
	-->
<cfset filePath = "https://uafanlc.alaska.edu/Online/">

</head>
<!-- 
clean and set variables sent from browse-resource1.cfm. 
-->
<cfoutput>
<cfif isDefined('ID')>
	<cfset ID = #Trim(ID)#>
<cfelse>
	<cfset ID = "">
</cfif>


<cfif isDefined("SUBMIT")>
	<cfset SUBMIT = #Trim(SUBMIT)#>
<cfelse>
	<cfset SUBMIT = "">
</cfif>

<p>ID = #ID#</P>
<cfquery name="GetURL" datasource="emeld">
		SELECT
		ddf.RESOURCEID,
		ddf.FILENAME
		FROM
		DATA_DIGITAL_FILE ddf
		WHERE
		ddf.DIGITALFILEID = <cfqueryparam value="#ID#" CFSQLType="CF_SQL_INTEGER">
		</cfquery>

<cfset ResID = GetURL.RESOURCEID>

<cfif SUBMIT EQ "Yes, I agree">

		
		<cfquery name="GetIdentifier" datasource="emeld">
			SELECT dr.Identifier FROM DATA_RESOURCE dr 
			WHERE dr.RESOURCEID = <cfqueryparam value="#ResID#" CFSQLType="CF_SQL_INTEGER">
		</cfquery>
		
		
<!-- audio IDs have been changed from ANLCXXXX to TIXXXX
however, still stored on snowy with ANLCXXXX names,
so we have to replace this in order to retrieve the file  gmh 10/21/05 -->

<!-- all files now storred on Bigdipper (ARSC) gmh 2017-03-31 -->
<!-- use regex to replace TIXXXX with ANLCXXXX in Identifier -->
		<cfset myIdentifier = REREplace(GetIdentifier.Identifier,"TI([0-9][0-9][0-9][0-9])","ANLC\1")>
		<cfset myURL = filePath & myIdentifier & "/" & getURL.FILENAME>
			
		<!---
		<cfif left(getURL.FILENAME,2) eq "TI">
			<cfset myURL = pdfpath   & getURL.FILENAME>
		<cfelse>
			<cfset myURL = audioPath  &  Replace(GetIdentifier.Identifier,"TI","")  & "/" & getURL.FILENAME>
		</cfif>
		--->
	
		<cflocation url="#myURL#"> 
	
<cfelse>
	<!--- if user does not agree, then return to browse page --->
	<cflocation url="browse-2.cfm?ResourceID=#GetURL.ResourceID#">


	</cfif>
	

	</cfoutput>

</html>
