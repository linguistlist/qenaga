<!--- print out affliated persons --->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Recording Titles</title>
</head>
<cfquery name="recordings" datasource="emeld">
SELECT RESOURCEID, FORMAT, TITLE, IDENTIFIER, OLDTITLE
FROM DATA_RESOURCE
WHERE FORMAT = n'Audio'
ORDER BY IDENTIFIER
</cfquery>

<body>

<h1>Recording Title</h1>
<table border=1>
<cfset count=0>

<cfoutput query="recordings">

<cfquery name="speakers" datasource="emeld">
SELECT DP.DATAPERSONLN LN, 
DP.DATAPERSONFN FN
FROM DATA_RESOURCE_PERSON DRP,DATA_PERSON DP
WHERE (DRP.DATAPERSONID = DP.DATAPERSONID) 
AND ( UPPER(DRP.DATAPERSONROLE) = n'SPEAKER')
AND (DRP.RESOURCEID = #recordings.RESOURCEID#)
ORDER BY LN, FN
</cfquery>

<cfif #speakers.recordcount# gt 0>
<cfset newTitle = "">
<cfloop query="speakers">
<cfif #FN# eq "-">
	<cfset newtitle = newtitle & "; "  & #LN#>
<cfelse>
	<cfset newtitle = newtitle & "; " & #FN# & " " & #LN#>
</cfif>
</cfloop>
<cfset newtitle = "[Recording: " & right(#newtitle#,len(#newtitle#)-2) & "]">
<cfelse>
<cfset newtitle = "[#recordings.TITLE#]"> 
<cfset count=count+1>
</cfif>

<!---
Don't urn this until OLDTITLE column created ... just in case 

<cfquery name="doit" datasource="emeld">
UPDATE DATA_RESOURCE
SET TITLE = n'#NEWTITLE#'
WHERE RESOURCEID = #RECORDINGS.RESOURCEID#
</cfquery>
--->

<tr valign="top">
<td>#RESOURCEID#</td>
<td>#IDENTIFIER#</td>
<td>#OLDTITLE#</td>
<td>#newTitle#</td>
</tr>
</cfoutput>
</table>

<cfoutput>#count#</cfoutput>

</body>
</html>
