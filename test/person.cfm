
<cfquery name="getpeople" datasource="emeld">
SELECT 
dp.DATAPERSONID as ID,
dp.DATAPERSONLN as LN,
dp.DATAPERSONFN as FN
FROM DATA_PERSON dp
WHERE dp.DATAPERSONID IN
(SELECT DISTINCT drp.DATAPERSONID FROM DATA_RESOURCE_PERSON drp  
WHERE drp.DATAPERSONROLE = n'Speaker')
ORDER BY DATAPERSONLN
</cfquery>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title>Data Person</title>


</head>
<cfset count=0>
<body>
<h2>DATA Person (Speakers)</h2>
<table border="1">
<tr>
<td>ID</td><td>Last</td><td>First</td></tr>
<cfloop query="getpeople">
<cfset count=count+1>
<tr>
<cfoutput>
<td>#ID#</td>
<td>#LN#</td>
<td>#FN#</td>
</cfoutput>
</tr>


</cfloop>


</table>
<cfoutput><p>Total records: #count#</p></cfoutput>
</body>
</html>
