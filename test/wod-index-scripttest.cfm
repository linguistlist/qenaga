<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<title>Dena'ina Qenaga -- A Resource for the Dena'ina Language</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
<link rel="stylesheet" type="text/css" href="qenaga.css" />
<meta http-equiv="imagetoolbar" content="no" />
</head>
<!-- THIS IS THE WORKING SCRIPT -->


<cfquery name="getids" datasource="emeld">
select ID from DATA_WORDS 
</cfquery>
<cfset start_day = DateDiff("y","03/01/2006",Now())>
<!--- <cfset start_day = DayOfYear (Now())> --->
<cfset week = (start_day / 7)>
<cfset week = Round(#week#)>
<cfset L_wordIDs = ValueList(getids.id)>
<cfset takeword = ListGetAt(L_wordIDs, #week#)>
<!-- set the above variable to week when olga finishes list -->

<cfquery name="getword" datasource="emeld">
select 
	WORD,
	GLOSS,
	AUDIO
from DATA_WORDS
where ID=#takeword#
</cfquery>

<cfset audiofile = "audio/words/" & #getword.AUDIO#>






	<body>

	<div id="banner">
	<div id="nav">
		<ul>
			<li><a href="people.html">People</a></li>
			<li><a href="history.html">History</a></li>
			<li><a href="literature.html">Literature</a></li>
			<li><a href="learning.html">Learning</a></li>
			<li><a href="language.html" >Language</a></li>
			<li><a href="archive/index.cfm">Archive</a></li>	
			<li><a href="/">Home</a></li>
			
			
		</ul>
	</div>
	</div>
	<div id="leftblock">
	
	

		
			
			
		
			
			</tr>
		</table>
				<br />
	</div>

<div id="rightblock">
<cfoutput>
dayofyear#start_day#|week#week#|#takeword#|#getword.word#|#audiofile#|
</cfoutput>		
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</div>

<!--- footer for this page slightly different from that used for other pages --->
<table width="90%" align="center">
	<tr><td><br /><hr /></td></tr>
	<tr>
		<td align="justify">
<p class="fineprint">
Website development is supported by the Dena'ina Archiving, Training and Access project, funded by US National Science Foundation grant NSF-OPP 0326805. Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author and do not necessarily reflect the views of the National Science Foundation. See  <a href="credits.html" target="new">About this Site</a>  for additional information. 
</p>		
		
<p class="fineprint">
Materials on this site are copyrighted by the original authors, the speakers whose voices are recorded, and the Alaska
Native Language Center. 
There may be additional copyright holders as well as others who
have provided funding or support for work presented here.
Materials may be used freely for non-commercial, educational purposes as specified in the <a href="license.html">license
agreement</a>. Alaska Native Language Archive materials made available through the Dena'ina Qenaga Digital Archive may be subject to more
restrictive conditions of use as specified by the original depositors.
</p>
<p  class="fineprint">
Dena'ina Qenaga website copyright &copy; 2004-<cfoutput>#Year(NOW())#</cfoutput>. Suggestions for future development are welcome. If you have questions or comments about this site, please contact
the DATA Project: denaina [dot] qenaga [at] gmail [dot] com
</p>
		</td>
	</tr>
</table>		
	
</body>
</html>

